
'use strict';


class AbsProcess {
  static _setParamString(param, parameters) {
    if(parameters.has(param.name)) {
      param.value = parameters.get(param.name);
    }
  }
  
  static _setParamInteger(param, parameters) {
    if(parameters.has(param.name)) {
      param.value = Number.parseInt(parameters.get(param.name));
    }
  }
  
  static _setParamFloat(param, parameters) {
    if(parameters.has(param.name)) {
      param.value = Number.parseFloat(parameters.get(param.name));
    }
  }
  
  static run(settings, started) {
    const dynamicConfig = Reflect.get(global, 'dynamicConfig@abstractor');
    dynamicConfig.app = settings.name;
    if(settings.httpServers) {
      settings.httpServers.forEach((httpServer) => {
        const parameters = httpServer.parameters;
        AbsProcess._setParamString(parameters.host, dynamicConfig.parameters);
        AbsProcess._setParamInteger(parameters.port, dynamicConfig.parameters);
      });
    }
    if(settings.httpsServers) {
      settings.httpsServers.forEach((httpsServer) => {
        const parameters = httpsServer.parameters;
        AbsProcess._setParamString(parameters.host, dynamicConfig.parameters);
        AbsProcess._setParamInteger(parameters.port, dynamicConfig.parameters);
      });
    }
    if(settings.wsServers) {
      settings.wsServers.forEach((wsServer) => {
        const parameters = wsServer.parameters;
        AbsProcess._setParamString(parameters.host, dynamicConfig.parameters);
        AbsProcess._setParamInteger(parameters.port, dynamicConfig.parameters);
      });
    }
    const parent = settings.parent;
    AbsProcess._setParamString(parent.host, dynamicConfig.parameters);
    AbsProcess._setParamInteger(parent.port, dynamicConfig.parameters);
    
    const build = dynamicConfig.parameters.has('build');

    const LocalServer = require('z-abs-local-server/local-server');
    AbsProcess.process = new LocalServer(settings, build);
    
    const Gulp = Reflect.get(global, 'gulp@abstractor');
    const Solution = require('z-abs-project/project/solution');
    let solution = null;
    const silent = true;
    if(0 === dynamicConfig.tasks.length) {
      dynamicConfig.tasks.push('default');
    }
    dynamicConfig.tasks.forEach((task) => {
      Gulp.task(task, Gulp.series((cb) => {
        if(null === solution) {
          solution = new Solution(settings.name, task, build, dynamicConfig.parameters.has('dev'), dynamicConfig.parameters.get('bottleneck'), silent);
        }
        solution.load((err) => {
          cb(err);
          if(started) {
            started();
          }
        }, `task:${task}`);
      }));
    });
    const GitTasks = require('./project/server/git-tasks');
  }
}


AbsProcess.process = null;


module.exports = AbsProcess;
