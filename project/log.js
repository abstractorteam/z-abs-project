
'use strict';


class Log {
  static start(name, startDate) {
    console.log('[' + `${startDate.getHours()}`.padStart(2, '0').grey + ':'.grey + `${startDate.getMinutes()}`.padStart(2, '0').grey + ':'.grey + `${startDate.getSeconds()}`.padStart(2, '0').grey + '] Starting \'' + name.cyan + '\'');
  }
  
  static end(name, startDate, stopDate) {
    const diff = stopDate - startDate;
    const milliseconds = parseInt((diff%1000));
    const seconds = parseInt((diff/1000)%60);
    const minutes = parseInt((diff/(1000*60))%60);
    const hours = parseInt((diff/(1000*60*60))%24);
    let diffResult = '';
    if(hours > 0) {
      diffResult += `${hours}h `;
      diffResult += `${minutes}min `;
      diffResult += `${seconds}s `;
      diffResult += `${milliseconds}ms`;
    }
    else if(minutes > 0) {
      diffResult += `${minutes}min `;
      diffResult += `${seconds}s `;
      diffResult += `${milliseconds}ms`;
    }
    else if(seconds > 0) {
      diffResult += `${seconds}s `;
      diffResult += `${milliseconds}ms`;
    }
    else {
      diffResult += `${milliseconds}ms`;
    }
    console.log('[' + `${stopDate.getHours()}`.padStart(2, '0').grey + ':'.grey + `${stopDate.getMinutes()}`.padStart(2, '0').grey + ':'.grey + `${stopDate.getSeconds()}`.padStart(2, '0').grey + '] Finished \'' + name.cyan + '\' after', `${diffResult}`.magenta);
    
  }
}


module.exports = Log;
