
'use strict';

const Gulp = require('gulp');
const GulpCached = require('gulp-cached');
const GulpChanged = require('gulp-changed');
const GulpPlumber = require('gulp-plumber'); 
const GulpPrint = require('gulp-print').default;
const Path = require('path');


class ChildServerRc {
  constructor(name, silent, repo, source, dest, nodeEnv) {
    this.name = name;
    this.silent = silent;
    this.repo = repo;
    this.source = source;
    this.dest = dest;
    process.env.NODE_ENV = nodeEnv;
  }
  
  task(cb) {
    const resolvedPath = Path.resolve(this.repo ? Path.resolve(this.repo) : '.');
    Gulp.src(this.source, {cwd: resolvedPath})
    .on('error', console.error.bind(console))
    .pipe(GulpCached(this.name))
    .pipe(GulpChanged(this.dest))
    .pipe(GulpPlumber())
    .pipe(Gulp.dest(this.dest))
    .pipe(GulpPrint((filepath) => {
      if(!this.silent) {
        return `${this.name}: ${filepath}`;
      }
    }))
    .on('end', cb);
  }
}

const parameters = JSON.parse(process.argv[2]);

const childServerRc = new ChildServerRc(...parameters);
childServerRc.task(() => {});

