
'use strict';

const Project = require('./project');
const ChildProcess = require('child_process');
const Path = require('path');


class ProjectServerJs extends Project {
  constructor(layer, part, source, dest, replaceCondition, replaceHandle) {
    super(`Server:${layer}Layer/${part}`, __filename, source);
    this.layer = layer;
    this.part = part;
    this.dest = dest;
    this.replaceCondition = replaceCondition ? (new RegExp(replaceCondition)).source : null;
    this.replaceHandle = replaceHandle ? ('string' === typeof replaceHandle ? replaceHandle : replaceHandle.toString().replace(/^\W*(function[^{]+\{([\s\S]*)\}|[^=]+=>[^{]*\{([\s\S]*)\}|[^=]+=>(.+))/i, '$2$3$4')) : null;
  }
  
  saveData() {
    return {
      layer: this.layer,
      part: this.part,
      source: this.source,
      dest: this.dest,
      replaceCondition: this.replaceCondition,
      replaceHandle: this.replaceHandle
    };
  }
  
  task(cb) {
    const childProcess = ChildProcess.fork(require.resolve('./child-server-js.js'), [JSON.stringify([this.name, this.silent, this.repo, this.source, this.dest, process.env.NODE_ENV, this.replaceCondition, this.replaceHandle])], {execArgv: []});
    childProcess.on('exit', (code) => {
      this.changed(this.name);
      cb();
    });
  }
}

module.exports = ProjectServerJs;
