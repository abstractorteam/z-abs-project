
'use strict';

const Babelify = require('babelify');
const Browserify = require('browserify');
const Glob = require('glob');
const Gulp = require('gulp');
const GulpIf = require('gulp-if');
const GulpPlumber = require('gulp-plumber');
const GulpPrint = require('gulp-print').default;
const GulpUglify = require('gulp-uglify');
const VinylBuffer = require('vinyl-buffer');
const VinylSourceStream = require('vinyl-source-stream');


class ChildClientBundle {
  constructor(name, silent, bundleName, source, dest, externals, externalGlobs, exports, exportSource, shim, nodeEnv) {
    this.name = name;
    this.silent = silent;
    this.bundleName = bundleName;
    this.source = source;
    this.dest = dest;
    this.externals = externals;
    this.externalGlobs = externalGlobs;
    this.exports = exports;
    this.exportSource = exportSource;
    this.shim = shim;
    process.env.NODE_ENV = nodeEnv;
  }
  
  task(cb) {
    const entries = [];
    let globPendings = 0;
    this.source.forEach((src) => {
      ++globPendings;
      Glob(src, (err, files) => {
        if(err) {
          console.log(err);
        }
        entries.push(files);
        if(0 === --globPendings) {
          const flatEntries = entries.flat();
          const browserify = Browserify({
            entries: flatEntries,
            extensions: ['.js'],
            debug: false,
            cache: {},
            packageCache: {}
          }, this.shim);
          
          if(null !== this.externals) {
            this.externals.forEach((external) => {
              browserify.external(external);
            });
          }
          
          if(null !== this.exports) {
            this.exports.forEach((export_) => {
              browserify.require(export_);
            });
          }
          
          let pendings = (this.externalGlobs ? this.externalGlobs.length : 0) + (this.exportSource ? 1 : 0);
          
          if(this.exportSource) {
            const start = './build/'.length;
            const stop = '.js'.length;
            flatEntries.forEach((entry) => {
              browserify.require(entry, {expose: entry.substring(start, entry.length - stop)});
            });
            if(0 === --pendings) {
              this._bundleClientBundle(browserify, cb);
            }
          }
          
          if(this.externalGlobs && 0 !== this.externalGlobs.length) {
            this.externalGlobs.forEach((externalGlob) => {
              Glob(externalGlob, (er, files) => {
                const start = './build/'.length;
                const stop = '.js'.length;
                files.forEach((file) => {
                  browserify.external(file.substring(start, file.length - stop));
                });
                if(0 === --pendings) {
                  this._bundleClientBundle(browserify, cb);
                }
              });
            });
          }
          
          if(0 === pendings) {
            this._bundleClientBundle(browserify, cb);
          }
        }
      });
    });
  }
  
  _bundleClientBundle(browserify, cb) {
    let compact = false;
    let minified = false;
    let comments = true;
    if(process.env.NODE_ENV === 'production') {
      compact = true;
      minified = true;
      comments = false;
    }
    browserify
    .on('error', console.error.bind(console))
    .transform(Babelify, {
      plugins: ["@babel/transform-runtime"],
      presets: ["@babel/preset-env"],
      compact: compact,
      minified: minified,
      comments: comments
    })
    .bundle()
    .pipe(VinylSourceStream(this.bundleName))
    .pipe(VinylBuffer())
    .pipe(GulpPlumber())
  //  .pipe(GulpIf(process.env.NODE_ENV === 'production', GulpUglify()))
    .pipe(Gulp.dest(this.dest))
    .pipe(GulpPrint((filepath) => {
      if(!this.silent) {
        return `${this.name}: ${filepath}`;
      }
    }))
    .on('end', cb);    
  }
}

const parameters = JSON.parse(process.argv[2]);

const childClientBundle = new ChildClientBundle(...parameters);
childClientBundle.task(() => {});

