
'use strict';

const Gulp = require('gulp');
const GulpBabel = require('gulp-babel');
const GulpCached = require('gulp-cached');
const GulpChanged = require('gulp-changed');
const GulpIf = require('gulp-if');
const GulpPlumber = require('gulp-plumber'); 
const GulpPrint = require('gulp-print').default;
const Path = require('path');


class ChildClientJs {
  constructor(name, silent, repo, source, dest, nodeEnv) {
    this.name = name;
    this.silent = silent;
    this.repo = repo;
    this.source = source;
    this.dest = dest;
    process.env.NODE_ENV = nodeEnv;
  }
  
  task(cb) {
    let compact = false;
    let minified = false;
    let comments = true;
    if(process.env.NODE_ENV === 'production') {
      compact = true;
      minified = true;
      comments = false;
    }
    const resolvedPath = Path.resolve(this.repo ? Path.resolve(this.repo) : '.');
    Gulp.src(this.source, {cwd: resolvedPath})
    .on('error', console.error.bind(console))
    .pipe(GulpCached(this.name))
    .pipe(GulpChanged(this.dest, {extension: '.js'}))
    .pipe(GulpPlumber())
    .pipe(GulpBabel({
      plugins: ["@babel/transform-runtime"],
      presets: ["@babel/react"],
      compact: compact,
      minified: minified,
      comments: comments
    }))
    .pipe(GulpIf(process.env.NODE_ENV === 'production', GulpBabel({
      presets: [["minify", {
        booleans: true,
        builtIns: false,
        consecutiveAdds: true,
        deadcode: true,
        evaluate: true,
        flipComparisons: true,
        guards: false,
        infinity: true,
        mangle: false,
        memberExpressions: true,
        mergeVars: false,
        numericLiterals: false,
        propertyLiterals: true,
        regexpConstructors: true,
        removeConsole: false,
        removeDebugger: false,
        removeUndefined: false,
        replace: false,
        simplify: false,
        simplifyComparisons: false,
        typeConstructors: false,
        undefinedToVoid: false
      }]]
    })))
    .pipe(Gulp.dest(this.dest))
    .pipe(GulpPrint((filepath) => {
      if(!this.silent) {
        return `${this.name}: ${filepath}`;
      }
    }))
    .on('end', cb);
  }
}

const parameters = JSON.parse(process.argv[2]);

const childClientJs = new ChildClientJs(...parameters);
childClientJs.task(() => {});

