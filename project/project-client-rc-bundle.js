
'use strict';

const Project = require('./project');
const ChildProcess = require('child_process');
const Path = require('path');


class ProjectClientRcBundle extends Project {
  constructor(part, source, dest) {
    super(`client-css-${part}-bundle`, __filename, source);
    this.part = part;
    this.bundleName = `bundle-${part}.css`;
    this.dest = dest;
  }
  
  saveData() {
    return {
      part: this.part,
      source: this.source,
      dest: this.dest
    };
  }
  
  task(cb) {
    const childProcess = ChildProcess.fork(require.resolve('./child-client-rc-bundle.js'), [JSON.stringify([this.name, this.silent, this.bundleName, this.repo, this.source, this.dest, process.env.NODE_ENV])], {execArgv: []});
    childProcess.on('exit', (code) => {
      this.changed(this.name);
      cb();
    });
  }
}

module.exports = ProjectClientRcBundle;
