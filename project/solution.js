
'use strict';

const Log = require('./log');
const BottleneckQueue = require('./bottleneck-queue');
const Project = require('./project');
const Task = require('./task');
const Colors = require('colors');
const Gulp = Reflect.get(global, 'gulp@abstractor');
const Fs = require('fs');
const Path = require('path');


class Solution {
  constructor(name, task, build, dev, bottleneck, silent = false) {
    this.name = name;
    this.task = task;
    this.dev = dev;
    this.build = build;
    this.bottleneck = bottleneck;
    this.silent = silent;
    this.projects = [];
    this.tasks = [];
    this.bottleneckQueue = new BottleneckQueue(bottleneck);
  }
  
  load(cbO, ...tasks) {
    const cb = () => {
      this.init();
      Gulp.series(...tasks)(cbO);
    };
    let solution = null;
    Fs.readFile(`.${Path.sep}workspace${Path.sep}${this.name}.wrk`, (err, data) => {
      if(err) {
        return console.log('solution.save ERROR:', err);
      }
      try {
        solution = JSON.parse(data);
      }
      catch(error) {
        return console.log('Could not load workspace.', error);
      }
      const dependencyNameMap = new Map();
      let pendings = 0;
      let currentProject = null;
      const done = () => {
        this.projects.forEach((project) => {
          currentProject = project;
          const dependencyNames = dependencyNameMap.get(project.name);
          dependencyNames.forEach((dependencyName) => {
            const dependentProject = this.getProject(dependencyName);
            if(!dependentProject) {
              console.log('Project'.red, project.name, 'can not load dependency project '.red + dependencyName + `.`.red);
            }
            else {
              project.addDependency(dependentProject);
            }
          });
        });
      };
      solution.projects.forEach((projectData) => {
        ++pendings;
        Project.load(projectData.fileName, projectData.requireName, this.bottleneckQueue, (err, project, dependencyNames) => {
          if(err) {
            return console.log(err);
          }
          if(null !== projectData.repo) {
            let repoPendings = 2;
            pendings += 2;
            let devRepoExists = false;
            let nodeModulesRepoExists = false;
            const devRepo = Path.resolve(`../${projectData.repoPath}/${projectData.repo}`);
            Fs.lstat(devRepo, (err, stat) => {
              devRepoExists = err ? false : stat.isDirectory();
              if(0 === --repoPendings) {
                project.setRepo(projectData.repo, projectData.repoPath, devRepoExists, nodeModulesRepoExists, this.dev);
              }
              if(0 === --pendings) {
                done();
                cb();
              }
            });            
            const nodeModulesRepo = Path.resolve(`./node_modules/${projectData.repo}`);
            Fs.lstat(nodeModulesRepo, (err, stat) => {
              nodeModulesRepoExists = err ? false : stat.isDirectory();
              if(0 === --repoPendings) {
                project.setRepo(projectData.repo, projectData.repoPath, devRepoExists, nodeModulesRepoExists, this.dev);
              }
              if(0 === --pendings) {
                done();
                cb();
              }
            });
          }
          else {
            project.setRepo(null);
          }
          dependencyNameMap.set(project.name, dependencyNames);
          this.addProject(project);
          if(0 === --pendings) {
            done();
            cb();
          }
        });
      });
      solution.tasks.forEach((taskData) => {
        ++pendings;
        Task.load(taskData.fileName, taskData.requireName, (err, task, dependencyNames) => {
          if(err) {
            return console.log(err);
          }
          dependencyNameMap.set(task.name, dependencyNames);
          this.addTask(task);
          if(0 === --pendings) {
            done();
            cb();
          }
        });
      });
    });
  }
  
  save() {
    const solution = {
      name: this.name,
      silent: this.silent,
      projects: [],
      tasks: []
    };
    this.projects.forEach((project) => {
      solution.projects.push({
        name: project.name,
        fileName: project.fileName,
        requireName: project.requireName,
        repo: project.repoName
      });
      project.save();
    });
    this.tasks.forEach((task) => {
      solution.tasks.push({
        name: task.name,
        fileName: task.fileName,
        requireName: task.requireName
      });
      task.save();
    });
    Fs.writeFile('./workspace/actorjs.wrk', JSON.stringify(solution, null, 2), (err) => {
      if(err) {
        console.log('solution.save ERROR:', err);
      }
    });
  }
  
  addProject(project) {
    this.projects.push(project);
    project.solution = this;
    return project;
  }
  
  getProject(name) {
    return this.projects.find(project => project.name === name);
  }
  
  addTask(task) {
    this.tasks.push(task);
  }
  
  init() {
    const developmentTaskName = 'task:development';
    const productionTaskName = 'task:production';
    Gulp.task(developmentTaskName, (cb) => {
      process.env.NODE_ENV = 'development';
      process.nextTick(() => {
        cb();
      });
    });
    Gulp.task(productionTaskName, (cb) => {
      process.env.NODE_ENV = 'production';
      process.nextTick(() => {
        cb();
      });
    });
    this.tasks.forEach((task) => {
      task.init();
    });
    const startProjects = [];
    const watches = [];
    this.projects.forEach((project) => {
      project.init(this.silent);
      watches.push(project.getTaskWatch());
      if(0 === project.dependencies.length) {
        startProjects.push(project);
      }
    });
    const startTasks = [];
    startProjects.forEach((startProject) => {
      startTasks.push(startProject.getTaskChain());
    });
    const developmentTasks = [developmentTaskName, Gulp.parallel(...startTasks)];
    const productionTasks = [productionTaskName, Gulp.parallel(...startTasks)];
    if(!this.build) {
      developmentTasks.push(Gulp.parallel(...watches));
      productionTasks.push(Gulp.parallel(...watches));
    }
    Gulp.task(`${developmentTaskName}:solution`, (cb) => {
      const name = `${this.name} ${this.task}${this.build ? ' --build' : ''}${this.dev ? ' --dev' : ''}${this.bottleneck ? ' --bottleneck ' + this.bottleneck : ''}`;
      const startDate = new Date();
      Log.start(name, startDate);
      Gulp.series(developmentTasks)(() => {
        const stopDate = new Date();
        Log.end(name, startDate, stopDate);
        cb();
      });
    });
    Gulp.task(`${productionTaskName}:solution`, (cb) => {
      const name = `${this.name} ${this.task}${this.build ? ' --build' : ''}${this.dev ? ' --dev' : ''}${this.bottleneck ? ' --bottleneck ' + this.bottleneck : ''}`;
      const startDate = new Date();
      Log.start(name, startDate);
      Gulp.series(productionTasks)(() => {
        const stopDate = new Date();
        Log.end(name, startDate, stopDate);
        cb();
      });
    });
    Gulp.task(`task:watch`, (cb) => {
      const name = `${this.name} ${this.task}${this.build ? ' --build' : ''}${this.dev ? ' --dev' : ''}${this.bottleneck ? ' --bottleneck ' + this.bottleneck : ''}`;
      const startDate = new Date();
      Log.start(name, startDate);
      Gulp.parallel(...watches)(() => {
        const stopDate = new Date();
        Log.end(name, startDate, stopDate);
        cb();
      });
    });
  }
}

module.exports = Solution;
