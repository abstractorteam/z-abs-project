
'use strict';

const Task = require('./task');
const Fs = require('fs');
const Path = require('path');


class TaskSimple extends Task {
  constructor(part, handle, parameters) {
    super(part, __filename);
    this.part = part;
    this.handle = handle ? ('string' === typeof handle ? handle : handle.toString().replace(/^\W*(function[^{]+\{([\s\S]*)\}|[^=]+=>[^{]*\{([\s\S]*)\}|[^=]+=>(.+))/i, '$2$3$4')) : null;
    this.handleFunction = null;
    this.handleFile = '';
    if(parameters && 1 === parameters.length) {
      this.handleFile = parameters[0];
    }
    else {
      this.handleFunction = new Function('cb', this.handle);
    }
  }
  
  saveData() {
    return {
      part: this.part,
      handle: this.handle,
      parameters: this.parameters
    }; 
  }
  
  task(cb) {
    if(null !== this.handle) {
      if(null !== this.handleFunction) {
        this.handleFunction(() => {
          cb();
        });
      }
      else {
        Fs.readFile(this.handleFile, (err, data) => {
          const handleText = data.toString();
          this.handle = this.handle.replace('${text}', handleText);
           this.handleFunction = new Function('cb', this.handle);
          this.handleFunction(() => {
            cb();
          });
        });
      }
    }
    else {
      process.nextTick(() => {
        cb();
      });
    }
  }
}

module.exports = TaskSimple;
