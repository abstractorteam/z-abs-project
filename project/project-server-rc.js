
'use strict';

const Project = require('./project');
const ChildProcess = require('child_process');
const Path = require('path');


class ProjectServerRc extends Project {
  constructor(part, source, dest) {
    super(`server-${part}-rc`, __filename, source);
    this.part = part;
    this.dest = dest;
  }
  
  saveData() {
    return {
      part: this.part,
      source: this.source,
      dest: this.dest
    };
  }
  
  task(cb) {
    const childProcess = ChildProcess.fork(require.resolve('./child-server-rc.js'), [JSON.stringify([this.name, this.silent, this.repo, this.source, this.dest, process.env.NODE_ENV])], {execArgv: []});
    childProcess.on('exit', (code) => {
      this.changed(this.name);
      cb();
    });
  }
}

module.exports = ProjectServerRc;
