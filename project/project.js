
'use strict';

const Log = require('./log');
const Colors = require('colors');
const Gulp = Reflect.get(global, 'gulp@abstractor');
const Fs = require('fs');
const Path = require('path');


class Project {
  constructor(name, fileName, source) {
    this.bottleneckQueue = null;
    this.name = name;
    this.fileName = name.replace(/[/\\?%*:|"<>]/g, '-');
    this.requireName = fileName.substring(__dirname.length + 1, fileName.length - '.js'.length);
    this.watchName = `watch:${name}`;
    this.source = [];
    this.isBundle = false;
    if(Array.isArray(source)) {
      source.forEach((src) => {
        if('object' === typeof src) {
          this.source.push(src.path + src.filter);
        }
        else if('string' === typeof src) {
          this.source.push(src);
        }
      });
    }
    else if('object' === typeof source) {
      this.source.push(source.path + source.filter);
    }
    else if('string' === typeof source) {
      this.source.push(source);
    }
    this.repo = null;
    this.repoPath = '.';
    this.silent = false;
    this.solution = null;
    this.dependencies = [];
    this.parents = [];
    this.executingChildren = 0;
    this.watchTrigger = false;
    this.haveChildrenChanged = false;
    this.initialized = false;
    this.taskChain = null;
    this.requestDate = null;
    this.startDate = null;
  }
  
  setBottleneckQueue(bottleneckQueue) {
    this.bottleneckQueue = bottleneckQueue;
  }
  
  setRepo(repo, repoPath, devRepoExists, nodeModulesRepoExists, dev) {
    if(null == repo) {
      this.repo = './';
    }
    else if(dev) {
      if(devRepoExists) {
        this.repo =  `../${repoPath}/${repo}`;
      }
      else if(nodeModulesRepoExists) {
        this.repo = `./node_modules/${repo}`;
      }
      else {
        console.log(`--dev - Repo ${repo} is missing.`);
      }
    }
    else {
      if(nodeModulesRepoExists) {
        this.repo = `./node_modules/${repo}`;
      }
      else if(devRepoExists) {
         this.repo =  `../${repoPath}/${repo}`;
      }
      else {
        console.log(`Repo ${repo} is missing.`);
      }
    }
    this.repoPath = repoPath;
  }

  save() {
    const data = this.saveData();
    Reflect.set(data, 'dependencyNames', this.dependencies.map((dependency) => dependency.name));
    Fs.writeFile(`./workspace/${this.fileName}.prj`, JSON.stringify(data, null, 2), ((err) => {
      if(err) {
        console.log('ERROR:'. err);
      }
    }));
  }
  
  static parseFile(data, fileName) {
    try {
      const dataObject = JSON.parse(data);
      return dataObject;
    } 
    catch(e) {
      console.log(`file: '${fileName}' - JSON.parse error:`, e);
    }
  }
  
  static load(fileName, requireName, bottleneckQueue, cb) {
    Fs.readFile(`./workspace/${fileName}.prj`, (err, data) => {
      if(err) {
        return cb(err);
      }
      const dataObject = Project.parseFile(data, fileName);
      const keys = Reflect.ownKeys(dataObject);
      const parameters = [];
      let dependencyNames = [];
      keys.forEach((key) => {
        if('dependencyNames' !== key) {
          if(!key.startsWith('_')) {
            parameters.push(Reflect.get(dataObject, key));
          }
          else {
            
          }
        }
        else {
          dependencyNames = Reflect.get(dataObject, key);
        }
      });
      const P = require('./' + requireName);
      const project = new P(...parameters);
      project.setBottleneckQueue(bottleneckQueue);
      project.isBundle = undefined !== dataObject._bundle ? dataObject._bundle : false;
      cb(err, project, dependencyNames);
    });
  }
  
  init(silent) {
    if(!this.initialized) {
      this.silent = silent;
      Gulp.task(this.name, this.executeTask.bind(this));
      Gulp.task(this.watchName, this.watch.bind(this));
      this.initialized  = true;
    }
  }
  
  addDependency(dependency) {
    this.dependencies.push(dependency);
    dependency.parents.push(this);
  }
  
  executeTask(cb) {
    if(0 === this.executingChildren && (this.watchTrigger || 0 === this.dependencies.length || this.haveChildrenChanged)) {
      const requestDate = new Date();
      if(null === this.startDate) {
        if(null === this.requestDate) {
          this.requestDate = requestDate;
        }
        else {
          process.nextTick(() => {
            cb();
          });
          return;
        }
      }
      this.bottleneckQueue.execute((cbExec) => {
        const startDate = this.startDate = new Date();
        this.started(this.name, startDate);
        this.task(() => {
          this.requestDate = null;
          this.startDate = null;
          this.stopped(this.name, startDate, new Date());
          cbExec();
          cb();
        });
      });
    }
    else {
      process.nextTick(() => {
        cb();
      });
    }
  }
  
  watch(cb) {
    if(0 !== this.source.length) {
      const resolvedPath = Path.resolve(this.repo ? Path.resolve(this.repo) : '.');
      Gulp.watch(this.source, {cwd: resolvedPath}, (cb) => {
        this.watchTrigger = true;
        Gulp.series(this.name)(cb);
      }).on('unlink', (path, stats) => {
        //console.log(`File ${path} was removed`);
        if(!this.isBundle) {
          this._removeFile(path, this.source, this.dest);
        }
      });
    }
    process.nextTick(() => {
      cb();
    });
  }
  
  getTaskWatch() {
    return this.watchName;
  }
  
  getTaskChain() {
    if(null === this.taskChain) {
      const parentTasks = [];
      this.parents.forEach((parent) => {
        parentTasks.push(parent.getTaskChain());
      });
      if(0 !== parentTasks.length) {
        this.taskChain = Gulp.series(this.name, Gulp.parallel(...parentTasks));
      }
      else {
        this.taskChain = Gulp.series(this.name);
      }
    }
    return this.taskChain;
  }
  
  started(name, startDate) {
    Log.start(name, startDate);
    ++Project.runningTasks;
    this.parents.forEach((parent) => {
      parent.childStarted(name);
    });
  }
  
  stopped(name, startDate, stopDate) {
    const diff = stopDate - startDate;
    Log.end(name, startDate, stopDate);
    this.parents.forEach((parent) => {
      parent.childStopped(name);
    });
    this.haveChildrenChanged = false;
    this.watchTrigger = false;
    setTimeout(() => {
      if(0 === --Project.runningTasks) {
        const current = Path.resolve('.');
        Object.keys(require.cache).forEach((key) => {
          if(key.startsWith(`${current}${Path.sep}dist`)) {
            delete require.cache[key];
          }
        });
      }
    });
  }
  
  changed(name) {
    this.parents.forEach((parent) => {
      parent.childChanged(name);
    });
  }
  
  childStarted(name) {
    ++this.executingChildren;
    this.parents.forEach((parent) => {
      parent.childStarted(name);
    });
  }
  
  childStopped(name) {
    this.parents.forEach((parent) => {
      parent.childStopped(name);
    });
    --this.executingChildren;
  }
  
  childChanged(name) {
    this.parents.forEach((parent) => {
      parent.childChanged(name);
    });
    this.haveChildrenChanged = true;
  }
  
  _removeFile(file, srcs, build, decrement = 0) {
    let removeFile = null;
    srcs.forEach((src) => {
      const indexOfStar = src.indexOf('*');
      if(-1 !== indexOfStar) {
        const formattedSrc = Path.normalize(src.substring(0, indexOfStar - 1));
        if(file.startsWith(formattedSrc)) {
          removeFile = `${build}${file.substring(formattedSrc.length)}`.replace(new RegExp('[/\\\\]', 'g'), Path.sep);
        }
      }
    });
    if(0 !== decrement) {
      removeFile = removeFile.substring(0, removeFile.length - decrement);
    }
    Fs.unlink(removeFile, (err) => {
      if(err) {
        console.log(`Could not remove watched file: '${removeFile}' from dist.`, err);
      }
    });
  }
}

Project.runningTasks = 0;

module.exports = Project;
