
'use strict';

const Gulp = require('gulp');
const GulpConcat = require('gulp-concat');
const GulpPlumber = require('gulp-plumber'); 
const GulpPrint = require('gulp-print').default;
const Path = require('path');


class ChildClientRcBundle {
  constructor(name, silent, bundleName, repo, source, dest, nodeEnv) {
    this.name = name;
    this.silent = silent;
    this.bundleName = bundleName;
    this.repo = repo;
    this.source = source;
    this.dest = dest;
    process.env.NODE_ENV = nodeEnv;
  }
  
  task(cb) {
    const resolvedPath = Path.resolve(this.repo ? Path.resolve(this.repo) : '.');
    Gulp.src(this.source, {cwd: resolvedPath})
    .on('error', console.error.bind(console))
    //.pipe(gulpCache(gulpCommands.compileCss))
    //.pipe(gulpChanged(config.paths.dist + '/css'))
    .pipe(GulpConcat(this.bundleName))
    .pipe(GulpPlumber())
    .pipe(Gulp.dest(this.dest))
    .pipe(GulpPrint((filepath) => {
      if(!this.silent) {
        return `${this.name}: ${filepath}`;
      }
    }))
    .on('end', cb);
  }
}

const parameters = JSON.parse(process.argv[2]);

const childClientRcBundle = new ChildClientRcBundle(...parameters);
childClientRcBundle.task(() => {});
