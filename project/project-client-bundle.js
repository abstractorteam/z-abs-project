
'use strict';

const Project = require('./project');
const ChildProcess = require('child_process');
const Path = require('path');


class ProjectClientBundle extends Project {
  constructor(layer, part, source, dest, externals, externalGlobs, exports, exportSource, shim) {
    super(`Bundle:${layer}Layer/${part}`, __filename, source);
    this.layer = layer;
    this.part = part;
    this.bundleName = `${layer}Layer-${part}.js`;
    this.dest = dest;
    this.externals = externals;
    this.externalGlobs = externalGlobs;
    this.exports = exports;
    this.exportSource = exportSource;
    this.shim = shim;
  }
  
  saveData() {
    return {
      layer: this.layer,
      part: this.part,
      source: this.source,
      dest: this.dest,
      externals: this.externals,
      externalGlobs: this.externalGlobs,
      exports: this.exports,
      exportSource: this.exportSource,
      shim: this.shim
    };
  }
  
  task(cb) {
    const childProcess = ChildProcess.fork(require.resolve('./child-client-bundle.js'), [JSON.stringify([this.name, this.silent, this.bundleName, this.source, this.dest, this.externals, this.externalGlobs, this.exports, this.exportSource, this.shim, process.env.NODE_ENV])], {execArgv: []});
    childProcess.on('exit', (code) => {
      this.changed(this.name);
      cb();
    });
  }
}

module.exports = ProjectClientBundle;
