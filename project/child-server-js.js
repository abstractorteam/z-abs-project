
'use strict';

const Gulp = require('gulp');
const GulpCached = require('gulp-cached');
const GulpChanged = require('gulp-changed');
const GulpIf = require('gulp-if');
const GulpPlumber = require('gulp-plumber'); 
const GulpPrint = require('gulp-print').default;
const GulpReplace = require('gulp-replace');
const Path = require('path');


class ChildServerJs {
  constructor(name, silent, repo, source, dest, nodeEnv, replaceCondition, replaceHandle) {
    this.name = name;
    this.silent = silent;
    this.repo = repo;
    this.source = source;
    this.dest = dest;
    this.replaceCondition = null !== replaceCondition ? new RegExp(replaceCondition, 'g') : null;
    this.replaceHandle = null !== replaceHandle ? new Function('match', 'parameters', replaceHandle) : null;
    process.env.NODE_ENV = nodeEnv;
  }
  
  task(cb) {
    const resolvedPath = Path.resolve(this.repo ? Path.resolve(this.repo) : '.');
    Gulp.src(this.source, {cwd: resolvedPath})
    .on('error', console.error.bind(console))
    .pipe(GulpCached(this.name))
    .pipe(GulpChanged(this.dest))
    .pipe(GulpPlumber())
    .pipe(GulpIf(null !== this.replaceHandle, GulpReplace(this.replaceCondition, (match, ...parameters) => {
      parameters.pop();
      parameters.pop();
      return this.replaceHandle(match, parameters);
    })))
    .pipe(Gulp.dest(this.dest))
    .pipe(GulpPrint((filepath) => {
      if(!this.silent) {
        return `${this.name}: ${filepath}`;
      }
    }))
    .on('end', cb);
  }
}

const parameters = JSON.parse(process.argv[2]);

const childServerJs = new ChildServerJs(...parameters);
childServerJs.task(() => {});

