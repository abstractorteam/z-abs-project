
'use strict';

const Project = require('./project');
const ChildProcess = require('child_process');
const Path = require('path');


class ProjectClientJs extends Project {
  constructor(layer, part, source, dest) {
    super(`Client:${layer}Layer/${part}`, __filename, source);
    this.layer = layer;
    this.part = part;
    this.dest = dest;
  }
  
  saveData() {
    return {
      layer: this.layer,
      part: this.part,
      source: this.source,
      dest: this.dest
    };
  }
  
  task(cb) {
    const childProcess = ChildProcess.fork(require.resolve('./child-client-js.js'), [JSON.stringify([this.name, this.silent, this.repo, this.source, this.dest, process.env.NODE_ENV])], {execArgv: []});
    childProcess.on('exit', (code) => {
      this.changed(this.name);
      cb();
    });
  }
}

module.exports = ProjectClientJs;
