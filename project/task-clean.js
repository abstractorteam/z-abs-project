
'use strict';

const Task = require('./task');
const Fs = require('fs');
const Path = require('path');


class TaskClean extends Task {
  constructor(part, path, reqursive) {
    super(part, __filename);
    this.part = part;
    this.paths = Array.isArray(path) ? path : [path];
    this.reqursive = reqursive;
  }
  
  saveData() {
    return {
      part: this.part,
      paths: this.paths,
      reqursive: this.reqursive
    }; 
  }
  
  task(cb) {
    let pendings = 0;
    this.paths.forEach((path) => {
      ++pendings;
      this._rmdir(path, this.reqursive, (errors) => {
        if(0 === --pendings) {
          cb();
        }
      });
    });
  }
  
  _rmdir(path, recursive, done) {
    if(!recursive) {
      Fs.rmdir(path, (err) => {
        if(err) {
          return done([err]);
        }
        else {
          return done();
        }
      });
    }
    else {
      this._recursiveWalk(path, (errors) => {
        if(!errors) {
          Fs.rmdir(path, (err) => {
            return done();
          });
        }
        else {
          return done(errors);
        }
      }, (dir, done) => {
        Fs.rmdir(dir, (err) => {
          return done(err);
        });
      }, (file, done) => {
        Fs.unlink(file, (err) => {
          return done(err);
        });
      }, 0);
    }
  }
  
  _recursiveWalk(dir, done, handleDir, handleFile, index) {
    Fs.readdir(dir, (err, files) => {
      if(err) {
        return done([err]);
      }
      let pendings = files.length;
      let errors = [];
      if(0 === pendings) {
        return done();
      }
      files.forEach((file) => {
        const newDir = Path.resolve(dir, file);
        Fs.stat(newDir, (err, stat) => {
          if(err) {
            console.log('err', err);
            errors.push(err);
            if(0 === --pendings) {
              done(errors);
            }
          }
          else if(stat && stat.isDirectory()) {
            this._recursiveWalk(newDir, (rerrors) => {
              if(!rerrors) {
                handleDir(newDir, (herr) => {
                  if(herr) {
                    errors.push(herr);
                  }
                  if(0 === --pendings) {
                    done(0 !== errors.length ? errors : undefined);
                  }
                });
              }
              else {
                errors = errors.concat(rerrors);
                if(0 === --pendings) {
                  done(errors);
                }
              }
            }, handleDir, handleFile, ++index);
          }
          else if(stat && stat.isFile()) {
            handleFile(newDir, (err) => {
              if(err) {
                errors.push(err);
              }
              if(0 === --pendings) {
                done(0 !== errors.length ? errors : undefined);
              }
            });
          }
          else {
            if(0 === --pendings) {
              done(0 !== errors.length ? errors : undefined);
            }
          }
        });
      });  
    });
  }
}

module.exports = TaskClean;
