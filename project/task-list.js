
'use strict';

const Gulp = Reflect.get(global, 'gulp@abstractor');
const Task = require('./task');


class TaskList extends Task {
  constructor(part, list) {
    super(part, __filename, list);
    this.part = part;
    this.list = list;
  }
  
  saveData() {
    return {
      part: this.part,
      list: this.list
    }; 
  }
  
  task(cb) {
    Gulp.series(...this.list)(cb);
  }
}

module.exports = TaskList;
