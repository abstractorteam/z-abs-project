
'use strict';

const Colors = require('colors');
const Gulp = Reflect.get(global, 'gulp@abstractor');
const GitSimple = require('simple-git');
const GitGlobal = require('./git-global');
const RepoManager = require('./git/repo-manager');
const Fs = require('fs');
const Path = require('path');
const gitGlobal = new GitGlobal();
const _ReposJsonPath = Path.normalize(`..${Path.sep}Data${Path.sep}actorjs-data-global${Path.sep}repos.json`);
const _RepoManager = new RepoManager();
const MakeRelease = require('./git/make-release');
const TaskChain = require('./git/task-chain');


function printStatus(repo) {
  const result = repo.result;
  if(null === result) {
    console.log('  ' + repo.name + (repo.error ? `, error: ${repo.error}` : '') + (repo.isRepo ? '' : ' - is not a repo yet'.blue));
  }
  else {
    console.log('  ' + repo.name + (repo.error ? `, error: ${repo.error}` : '') + (repo.isRepo ? ' - On branch ' + result.current.cyan : ' - is not a repo yet.'));
    if(0 !== result.ahead) {
      console.log(`    Your branch is ahead of '${result.tracking}' by ` + `${result.ahead}`.magenta + ` commit${1 === result.ahead ? '' : 's'}.`);
    }
    if(0 !== result.behind) {
      console.log(`    Your branch is behind of '${result.tracking}' by ` + `${result.behind}`.magenta + ` commit${1 === result.behind ? '' : 's'}.`);
    }
    if(0 !== result.staged.length) {
      console.log('    Changes to be committed:');
      result.staged.forEach((staged) => {
        let status = '';
        if(undefined !== result.modified.find(element => element === staged)) {
          status = 'modified';
        }
        else if(undefined !== result.deleted.find(element => element === staged)) {
          status = 'deleted';
        }
        else if(undefined !== result.renamed.find(element => element === staged)) {
          status = 'renamed';
        }
        console.log(`      ${status}: ${staged}`.green);
      });
    }
    if(0 !== result.modified.length || 0 !== result.deleted.length || 0 !== result.renamed.length) {
      const modifiedNotStaged = [];
      const deletedNotStaged = [];
      const renamedNotStaged = [];
      result.modified.forEach((modified) => {
        if(undefined === result.staged.find(element => element === modified)) {
          modifiedNotStaged.push(modified);
        }
      });
      result.deleted.forEach((deleted) => {
        if(undefined === result.staged.find(element => element === deleted)) {
          deletedNotStaged.push(deleted);
        }
      });
      result.renamed.forEach((renamed) => {
        if(undefined === result.staged.find(element => element === renamed)) {
          renamedNotStaged.push(renamed);
        }
      });
      if(0 !== modifiedNotStaged.length || 0 !== deletedNotStaged.length || 0 !== renamedNotStaged.length) {
        console.log('    Changes not staged for commit:');
        result.modified.forEach((modified) => {
          console.log(`      modified: ${modified}`.red);
        });
        result.deleted.forEach((deleted) => {
          console.log(`      deleted: ${deleted}`.red);
        });
        result.renamed.forEach((renamed) => {
           console.log(`      renamed: ${renamed}`.red);
        });
      }
    }
    if(0 !== result.not_added.length) {
      console.log('    Untracked files:');
      result.not_added.forEach((not_added) => {
        console.log(`      ${not_added}`.red);
      });
    }
    if(0 !== result.conflicted.length) {
      console.log('    Untmerged paths:');
      result.conflicted.forEach((conflicted) => {
        console.log(`      ${conflicted}`.red);
      });
    }
  }
}

function printRepoStatus(repoGroups, heading = true) {
  if(heading) {
    console.log('******************************');
    console.log(`*** ${Reflect.get(global, 'dynamicConfig@abstractor').app} status `);
    console.log('******************************');
  }
  repoGroups.forEach((repoGroup) => {
    console.log(repoGroup.name + ' Repos');
    repoGroup.repos.forEach((repo) => {
      printStatus(repo);
    });
  });
}

function isRepo(path, cb) {
  const repoResult = {
    error: null,
    isFolder: false,
    isRepo: false,
    success: () => {
      return null === repoResult.error && repoResult.isFolder && repoResult.isRepo;
    }
  };
  Fs.lstat(path, (err, stat) => {
    if(err) {
      if('ENOENT' == err.code) {
        cb(repoResult);
      }
      else {
        repoResult.error = err;
        cb(err, repo);
      }
    }
    else if(stat.isDirectory()) {
      repoResult.isFolder = true;
      GitSimple(path).checkIsRepo((err, result) => {
        if(err) {
          repoResult.error = err;
        }
        else {
          repoResult.isRepo = true;
        }
        cb(repoResult);
      });
    }
    else {
      cb(repoResult);
    }
  });
}

function getRepoStatus(path, repoGroup, name, cb) {
  const repo = {
    name: name,
    path: path,
    error: null,
    isFolder: false,
    isRepo: false,
    ready: false,
    result: null
  };
  repoGroup.repos.push(repo);
  Fs.lstat(path, (err, stat) => {
    if(err) {
      if('ENOENT' == err.code) {
        cb(undefined, repo);
      }
      else {
        repo.error = err;
        cb(err, repo);
      }
    }
    else if(stat.isDirectory()) {
      repo.isFolder = true;
      GitSimple(path).checkIsRepo((err, result) => {
        if(err) {
          repo.error = err;
          return cb(err, repo);
        }
        if(result) {
          repo.isRepo = true;
          GitSimple(path).fetch((err, result) => {
            if(err) {
              repo.error = err;
              return cb(err, repo);
            }
            GitSimple(path).status((err, result) => {
              repo.error = err;
              repo.ready = 0 === result.not_added.length && 0 === result.conflicted.length && 0 === result.created.length && 0 === result.deleted.length && 0 === result.modified.length && 0 === result.renamed.length && 0 === result.staged.length && 0 === result.ahead && 0 == result.behind;
              repo.result = result;
              cb(undefined, repo);
            });
          });
        }
        else {
          cb(undefined, repo);
        }
      });
    }
    else {
      cb(undefined, repo);
    }
  });
}

function createRepoData() {
  const repoGroups = [];
  const build = {
    name: 'Build',
    repos: [],
    result: null
  };
  repoGroups.push(build);
  const development = {
    name: 'Development',
    repos: [],
    result: null
  };
  repoGroups.push(development);
  const content = {
    name: 'Content',
    repos: [],
    result: null
  };
  repoGroups.push(content);
  const data = {
    name: 'Data',
    repos: [],
    result: null
  };
  repoGroups.push(data);
  const documentation = {
    name: 'Documentation',
    repos: [],
    result: null
  };
  repoGroups.push(documentation);
  return {
    repoGroups,
    build,
    development,
    content,
    data,
    documentation
  };
}

Gulp.task('status', (cb) => {
  const dynamicConfig = Reflect.get(global, 'dynamicConfig@abstractor');
  const statusRepo = dynamicConfig.parameters.get('repo');
  const repoData = createRepoData() ;
  const dirTop = `${process.cwd()}${Path.sep}..`;
  Fs.readdir(dirTop, (err, files) => {
    if(err) {
      console.log(err);
      return;  
    }
    let pendings = 0;
    files.forEach((file) => {
      if(file.startsWith('actor')) {
        const path = `${dirTop}${Path.sep}${file}`;
        ++pendings;
        if(undefined === statusRepo || file === statusRepo) {
          getRepoStatus(path, repoData.build, file, () => {
            if(0 === --pendings) {
              printRepoStatus(repoData.repoGroups);
              cb();
            }
          });
        }
        else {
          process.nextTick(() => {
            if(0 === --pendings) {
              printRepoStatus(repoData.repoGroups);
              cb();
            } 
          });
        }
      }
      else if('Content' === file) {
        ++pendings;
        Fs.readdir(`${dirTop}${Path.sep}${file}`, (err, files) => {
          if(err) {
            if(0 === --pendings) {
              console.log(err);
              printRepoStatus(repoData.repoGroups);
              cb(err);
            }
            return;
          }
          files.forEach((file) => {
            ++pendings;
            if(undefined === statusRepo || file === statusRepo) {
               getRepoStatus(`${dirTop}${Path.sep}Content${Path.sep}${file}`, repoData.content, file, () => {
                if(0 === --pendings) {
                  printRepoStatus(repoData.repoGroups);
                  cb();
                }
              });
            }
            else {
              process.nextTick(() => {
                if(0 === --pendings) {
                  printRepoStatus(repoData.repoGroups);
                  cb();
                } 
              });
            }
          });
          if(0 === --pendings) {
            printRepoStatus(repoData.repoGroups);
            cb();
          }
        });
      }
      else if('Data' === file) {
        ++pendings;
        Fs.readdir(`${dirTop}${Path.sep}${file}`, (err, files) => {
          if(err) {
            if(0 === --pendings) {
              console.log(err);
              printRepoStatus(repoData.repoGroups);
              cb(err);
            }
            return;
          }
          files.forEach((file) => {
            ++pendings;
            if(undefined === statusRepo || file === statusRepo) {
              getRepoStatus(`${dirTop}${Path.sep}Data${Path.sep}${file}`, repoData.data, file, () => {
                if(0 === --pendings) {
                  printRepoStatus(repoData.repoGroups);
                  cb();
                }
              });
            }
            else {
              process.nextTick(() => {
                if(0 === --pendings) {
                  printRepoStatus(repoData.repoGroups);
                  cb();
                } 
              });
            }
          });
          if(0 === --pendings) {
            printRepoStatus(repoData.repoGroups);
            cb();
          }
        });
      }
      else if('Documentation' === file) {
        ++pendings;
        Fs.readdir(`${dirTop}${Path.sep}${file}`, (err, files) => {
          if(err) {
            if(0 === --pendings) {
              console.log(err);
              printRepoStatus(repoData.repoGroups);
              cb(err);
            }
            return;
          }
          files.forEach((file) => {
            ++pendings;
            if(undefined === statusRepo || file === statusRepo) {
              getRepoStatus(`${dirTop}${Path.sep}Documentation${Path.sep}${file}`, repoData.documentation, file, () => {
                if(0 === --pendings) {
                  printRepoStatus(repoData.repoGroups);
                  cb();
                }
              });
            }
            else {
              process.nextTick(() => {
                if(0 === --pendings) {
                  printRepoStatus(repoData.repoGroups);
                  cb();
                } 
              });
            }
          });
          if(0 === --pendings) {
            printRepoStatus(repoData.repoGroups);
            cb();
          }
        });
      }
      else if(file.startsWith('z-abs')) {
        const path = `${dirTop}${Path.sep}${file}`;
        ++pendings;
        if(undefined === statusRepo || file === statusRepo) {
          getRepoStatus(path, repoData.development, file, () => {
            if(0 === --pendings) {
              printRepoStatus(repoData.repoGroups);
              cb();
            }
          });
        }
        else {
          process.nextTick(() => {
            if(0 === --pendings) {
              printRepoStatus(repoData.repoGroups);
              cb();
            } 
          });
        }
      }
    });
  });
});

Gulp.task('diff', (cb) => {
  const dir = `${process.cwd()}${Path.sep}..`
  Fs.readdir(dir, (err, files) => {
    if(err) {
      console.log(err);
      return;  
    }
    files.forEach((file) => {
      const path = `${dir}${Path.sep}${file}`;
      Fs.lstat(path, (err, stat) => {
        if(err) {
          console.log(err);
          return;  
        }
        if(stat.isDirectory()) {
          GitSimple(path).checkIsRepo((err, result) => {
            if(err) {
              console.log(err);
              return;
            }
            if(result) {
              GitSimple(path).diff((err, result) => {
                console.log(file, result);
                /*result.files.forEach((file) => {
                  console.log('  ' + file.path);
                });*/
              });
            }
          });
        }
      });
    });
  });
  cb();
});

function printCloneResult(repoResults, cloneRepoResult, cloneRepo) {
  const globalTask = gitGlobal.getTaskName('Clone');
  let repoFound = false;
  let printText = ''  
  let cloneResult = true;
  repoResults.repoGroups.forEach((repoGroup) => {
    if(null === repoGroup.result) {
      return;
    }
    if(0 === repoGroup.result.repoSucceded.length && 0 === repoGroup.result.repoFailed.length && 0 === repoGroup.result.repoExists.length && 0 === repoGroup.result.folderExists.length) {
      return;
    }
    else {
      repoFound = true;
      printText += repoGroup.name + ' Repos\r\n';
    }
    if(0 !== repoGroup.result.repoSucceded.length) {
      printText += '  Cloned repos:'+ '\r\n';
      repoGroup.result.repoSucceded.forEach((repo) => {
        printText += `    ${repo.name}`.green+ '\r\n';
      });
    }
    if(0 !== repoGroup.result.repoFailed.length) {
      printText += '  Failed to clone repos:'+ '\r\n';
      repoGroup.result.repoFailed.forEach((repo) => {
        printText += `    ${repo.name}`.red+ '\r\n';
      });
      cloneResult = false;
    }
    if(0 !== repoGroup.result.repoExists.length) {
      printText += '  No clone when repo already exists, repos:'+ '\r\n';
      repoGroup.result.repoExists.forEach((repo) => {
        printText += `    ${repo.name}`.green+ '\r\n';
      });
    }
    if(0 !== repoGroup.result.folderExists.length) {
      printText += '  Can not clone to existing folders, repos:'+ '\r\n';
      repoGroup.result.folderExists.forEach((repo) => {
        printText += `    ${repo.name}`.red+ '\r\n';
      });
      cloneResult = false;
    }
  });
  if(cloneRepo && !repoFound) {
    printText += `The repo '` + cloneRepo.red + `' could not be found in the configuration.`;
    cloneResult = false;
  }
  if('Clone' === globalTask) {
    console.log('******************************');
    console.log('*** Clone result: ');
    console.log('******************************');
    console.log(printText);
  }
  else {
    if(cloneRepo) {
      gitGlobal.setResult({
        printText: printText,
        result: cloneRepoResult,
        ready: cloneResult
      });
    }
    else {
     gitGlobal.setResult({
        printText: printText,
        result: repoResults,
        ready: cloneResult
      });
    }
  }
}

Gulp.task('clone', (cb) => {
  const cloneAll = gitGlobal.getAll();
  const cloneRepo = gitGlobal.getRepo();
  let cloneRepoReady = false;
  let cloneRepoResult = null;
  
  let argumentErrors = [];
  if(!cloneRepo && !cloneAll) {
    argumentErrors.push('--repo [value] or --all must be set.');
  }
  
  if(0 !== argumentErrors.length) {
    let errorText = 'Clone error:'.red;
    argumentErrors.forEach((argumentError) => {
      errorText += '  ' + argumentError;
    });
    console.log(errorText);
    return cb(new Error(errorText));
  }
  
  Fs.readFile(_ReposJsonPath, (err, data) => {
    if(err) {
      const errText = `Could not read file: '${_ReposJsonPath}'`;
      console.err(errText);
      return cb(new Error(errText));
    }
    try {
      const repoDatas = JSON.parse(data);
      const repoResults = createRepoData();
      let ready = true;
      let pendings = 0;
      Reflect.ownKeys(repoDatas).forEach((key) => {
        const repoGroup = Reflect.get(repoDatas, key);
        repoGroup.repos.forEach((repoData) => {
          ++pendings;
          if(cloneAll || cloneRepo === repoData.folder) {
            const repoResult = Reflect.get(repoResults, key);
            repoResult.result = {
              folderExists: [],
              repoExists: [],
              repoSucceded: [],
              repoFailed: []
            };
            getRepoStatus(`${Path.normalize(repoGroup.path)}${Path.sep}${repoData.folder}`, repoResult, repoData.folder, (err, repoStatus) => {
              if(cloneRepo === repoStatus.name || cloneAll) {
                if(repoStatus.isFolder) {
                  if(repoStatus.isRepo) {
                    repoResult.result.repoExists.push(repoStatus);
                  }
                  else {
                    repoResult.result.folderExists.push(repoStatus);
                  }
                  ready = ready && repoStatus.ready && repoStatus.isRepo;
                  if(cloneRepo === repoStatus.name) {
                    cloneRepoReady = repoStatus.ready;
                    cloneRepoResult = repoStatus;
                  }
                }
                else {
                  console.log('start clone repo:', repoData.repo, '...');
                  ++pendings;
                  GitSimple().clone(repoData.repo, `${Path.normalize(repoGroup.path)}${Path.sep}${repoData.folder}`, (err, result) => {
                    if(!err) {
                      repoResult.result.repoSucceded.push(repoStatus);
                      cloneRepoReady = true;
                    }
                    else {
                      console.log(`Failed to clone repo `.red + `${Path.normalize(repoGroup.path)}${Path.sep}${repoData.folder}`, err);
                      repoResult.result.repoFailed.push(repoStatus);
                    }
                    ready = ready && !err;
                    if(cloneRepo === repoStatus.name) {
                      cloneRepoReady = repoStatus.ready;
                      cloneRepoResult = repoStatus;
                    }
                    if(0 === --pendings) {
                      gitGlobal.setReady(cloneAll ? ready : cloneRepoReady);
                      printCloneResult(repoResults, cloneRepoResult, cloneRepo);
                      cb();
                    }
                  });
                }
              }
              if(0 === --pendings) {
                gitGlobal.setReady(cloneAll ? ready : cloneRepoReady);
                printCloneResult(repoResults, cloneRepoResult, cloneRepo);
                cb();
              }
            });
          }
          else {
            process.nextTick(() => {
              if(0 === --pendings) {
                gitGlobal.setReady(cloneAll ? ready : cloneRepoReady);
                printCloneResult(repoResults, cloneRepoResult, cloneRepo);
                cb();
              }
            });
          }
        });
      });
    } catch(eeee) {
      console.log('aaaaaa eeee', eeee);
      cb();
    }
  });
});

Gulp.task('pull', (cb) => {
  _RepoManager.init(cb, () => {
    const tagAll = gitGlobal.getAll();
    const tagRepo = gitGlobal.getRepo();

    let argumentErrors = [];
    if(!tagRepo && !tagAll) {
      argumentErrors.push('--repo [value] or --all must be set.');
    }

    if(0 !== argumentErrors.length) {
      let errorText = 'git pull error:'.red;
      argumentErrors.forEach((argumentError) => {
        errorText += '  ' + argumentError;
      });
    
      console.log(errorText);
      return cb(new Error(errorText));
    }
  
    if(tagRepo) {
      const repoData = _RepoManager.get(tagRepo);
      if(repoData) {
        GitSimple(Path.normalize(`${repoData.path}${Path.sep}${repoData.data.folder}`)).pull((err, update) => {
          if(err) {
            console.log('git pull error:'.red, err);
          }
          else {
            console.log('git pull'.green, Path.normalize(`${repoData.path}${Path.sep}${repoData.data.folder}`));
          }
          cb(err);
        });
      }
      else {
        cb(new Error(''));
      }
    }
    else {
      let pendings = 0;
      const errors = [];
      _RepoManager.forEach((repoData) => {
        ++pendings;
        const folder = Path.normalize(`${repoData.path}${Path.sep}${repoData.data.folder}`);
        isRepo(folder, (result) => {
          if(result.success()) {
            GitSimple(folder).pull((err, update) => {
              if(err) {
                console.log('git pull error:'.red, err);
                errors.push('git pull error:'.red, err);
              }
              else {
                console.log('git pull'.green, Path.normalize(`${repoData.path}${Path.sep}${repoData.data.folder}`));
              }
              if(0 === --pendings) {
                cb(0 !== errors.length ? new Error(errors.join('\r\n')) : undefined);
              }
            });
          }
          else {
            if(result.error) {
              console.log(`Path ${folder} error: ${result.error}`.red);
            }
            else if(!result.isFolder) {
              console.log(`Path ${folder} does not exist.`.yellow);
            }
            else if(!result.isRepo) {
              console.log(`Path ${folder} is not a repo.`.yellow);
            }
            if(0 === --pendings) {
              cb(0 !== errors.length ? new Error(errors.join('\r\n')) : undefined);
            }
          }
        });
      });
    }  
  });
});

Gulp.task('tag', (cb) => {
  const globalTask = _globalTasksData && null !== _globalTasksData.originalTask;
  const dynamicConfig = Reflect.get(global, 'dynamicConfig@abstractor');
  const tagName = dynamicConfig.parameters.get('name');
  const tagMessage = dynamicConfig.parameters.get('message') ? dynamicConfig.parameters.get('message') : dynamicConfig.parameters.get('msg');
  
  const tagAll = gitGlobal.getAll();
  const tagRepo = gitGlobal.getRepo();
  
  let argumentErrors = [];
  if(!tagRepo && !tagAll) {
    argumentErrors.push('--repo [value] or --all must be set.');
  }
  if(!tagName) {
    argumentErrors.push('--name [value] must be set.');
  }
  
  if(!tagMessage) {
    argumentErrors.push('--msg [value] or --message [value] must be set.');
  }
  
  if(0 !== argumentErrors.length) {
    let errorText = 'Tag error:'.red;
    argumentErrors.forEach((argumentError) => {
      errorText += '  ' + argumentError;
    });
    
    console.log(errorText);
    return cb(new Error(errorText));
  }
  
  gitGlobal.init('Tag');
  
  if(tagAll) {
    console.log('Make sure repos are ready to Tag'.yellow);
  }
  else if(tagRepo) {
    console.log('Make sure repo '.yellow + tagRepo + ' are ready to Tag'.yellow);
  }
  
  Gulp.task('clone')((err) => {
    if(err) {
      return cb(err);
    }
    if(tagAll) {
      console.log('NOT IMPLEMENTED . repo: all, ready;');
    }
    else if(tagRepo) {
      const result = gitGlobal.getResult().result;
      if(null !== result) {
        if(gitGlobal.getReady()) {
          console.log('xxxxxxxxxxxxxxxxxxxxxxxxxxx', gitGlobal.getReady());
          console.log(Path.resolve(result.path));
          /*GitSimple(result.path).addAnnotatedTag(tagName, tagMessage, (err, tagResult) => {
            console.log(err);
            console.log(tagResult);
            if(!err) {
              console.log(result.name + ' added Tag '.yellow + tagName + ' with message "'.yellow + tagMessage);
            }
            return cb(err);
          });*/
        }
        else {
          printStatus(result);
          console.log(result.name + ' are not ready to Tag'.yellow);
          return cb();
        }
      }
      else {
        console.log(gitGlobal.getResult().printText);
        console.log(err);
        return cb(err);
      }
    }
    else {
      cb(err);
    }
  });
});

Gulp.task('major', (cb) => {
  const makeRelease = new MakeRelease(Reflect.get(global, 'dynamicConfig@abstractor').app, 'Major', _RepoManager, printStatus);
  makeRelease.execute(() => {
    new TaskChain(makeRelease.startRelease, undefined, makeRelease, cb)
     .pipe(makeRelease.doesAllRepoPathsExist, [makeRelease.createNoneExistingRepoPaths, undefined, undefined])
     .pipe(makeRelease.doesAllRepoPathsContainARepo, [makeRelease.cloneNoneExistingRepo, undefined, undefined])
     .pipe(makeRelease.gitStatus)
     .pipe(makeRelease.readPackageJson)
     .pipe(makeRelease.updateVersionInPackageJson)
     .pipe(makeRelease.writePackageJson)
     .pipe(makeRelease.gitAddPackageJson)
     .pipe(makeRelease.gitCommitPackageJson)
     .pipe(makeRelease.npmInstallRepos)
     .pipe(makeRelease.gitAddPackageLockJson)
     .pipe(makeRelease.gitCommitPackageLockJson)
     .pipe(makeRelease.gitPushPackageJson)
     .pipe(makeRelease.gitTagPackageJson)
     .pipe(makeRelease.gitPushTagPackageJson);
  }, cb);
});

Gulp.task('minor', (cb) => {
  const makeRelease = new MakeRelease(Reflect.get(global, 'dynamicConfig@abstractor').app, 'Minor', _RepoManager, printStatus);
  makeRelease.execute(() => {
    new TaskChain(makeRelease.startRelease, undefined, makeRelease, cb)
     .pipe(makeRelease.doesAllRepoPathsExist, [makeRelease.createNoneExistingRepoPaths, undefined, undefined])
     .pipe(makeRelease.doesAllRepoPathsContainARepo, [makeRelease.cloneNoneExistingRepo, undefined, undefined])
     .pipe(makeRelease.gitStatus)
     .pipe(makeRelease.readPackageJson)
     .pipe(makeRelease.updateVersionInPackageJson)
     .pipe(makeRelease.writePackageJson)
     .pipe(makeRelease.gitAddPackageJson)
     .pipe(makeRelease.gitCommitPackageJson)
     .pipe(makeRelease.npmInstallRepos)
     .pipe(makeRelease.gitAddPackageLockJson)
     .pipe(makeRelease.gitCommitPackageLockJson)
     .pipe(makeRelease.gitPushPackageJson)
     .pipe(makeRelease.gitTagPackageJson)
     .pipe(makeRelease.gitPushTagPackageJson);
  }, cb);
});

Gulp.task('patch', (cb) => {
  const makeRelease = new MakeRelease(Reflect.get(global, 'dynamicConfig@abstractor').app, 'Patch', _RepoManager, printStatus);
  makeRelease.execute(() => {
    new TaskChain(makeRelease.startRelease, undefined, makeRelease, cb)
     .pipe(makeRelease.doesAllRepoPathsExist, [makeRelease.createNoneExistingRepoPaths, undefined, undefined])
     .pipe(makeRelease.doesAllRepoPathsContainARepo, [makeRelease.cloneNoneExistingRepo, undefined, undefined])
     .pipe(makeRelease.gitStatus)
     .pipe(makeRelease.readPackageJson)
     .pipe(makeRelease.updateVersionInPackageJson)
     .pipe(makeRelease.writePackageJson)
     .pipe(makeRelease.gitAddPackageJson)
     .pipe(makeRelease.gitCommitPackageJson)
     .pipe(makeRelease.npmInstallRepos)
     .pipe(makeRelease.gitAddPackageLockJson)
     .pipe(makeRelease.gitCommitPackageLockJson)
     .pipe(makeRelease.gitPushPackageJson)
     .pipe(makeRelease.gitTagPackageJson)
     .pipe(makeRelease.gitPushTagPackageJson);
  }, cb);
});

Gulp.task('publish', (cb) => {
  const makeRelease = new MakeRelease(Reflect.get(global, 'dynamicConfig@abstractor').app, 'Publish', _RepoManager, printStatus);
  makeRelease.execute(() => {
    new TaskChain(makeRelease.startPublish, undefined, makeRelease, cb)
      .pipe(makeRelease.readPackageJson)
      .pipe(makeRelease.publishFolderExists)
      .pipe(makeRelease.createPublishFolder)
      .pipe(makeRelease.clonePublishRepos)
      .pipe(makeRelease.findFilesFromPublishRepo)
      .pipe(makeRelease.removeFilesFromPublishRepo)
      .pipe(makeRelease.copyFilesToPublishRepo)
      .pipe(makeRelease.createPublishPackageJsonFromReleasePackageJson)
      .pipe(makeRelease.readPublishPackageJson)
      .pipe(makeRelease.updatePublishPackageJson)
      .pipe(makeRelease.updateAbstractorDependenciesInPublishPackageJson)
      .pipe(makeRelease.writePublishPackageJson)
      .pipe(makeRelease.npmInstallPublishRepos, undefined, MakeRelease.DEP)
      .pipe(makeRelease.gitAddPublish, MakeRelease.DEP)
      .pipe(makeRelease.gitCommitPublish, MakeRelease.DEP)
      .pipe(makeRelease.gitPushPublish, undefined, MakeRelease.DEP)
      .pipe(makeRelease.gitTagPublish, undefined, MakeRelease.DEP)
      .pipe(makeRelease.gitPushTagPublish, undefined, MakeRelease.DEP)
      .pipe(makeRelease.npmInstallPublishRepos, undefined, MakeRelease.APP)
      .pipe(makeRelease.gitAddPublish, MakeRelease.APP)
      .pipe(makeRelease.gitCommitPublish, MakeRelease.APP)
      .pipe(makeRelease.gitPushPublish, undefined, MakeRelease.APP)
      .pipe(makeRelease.gitTagPublish, undefined, MakeRelease.APP)
      .pipe(makeRelease.gitPushTagPublish, undefined, MakeRelease.APP);
  }, cb);
});
