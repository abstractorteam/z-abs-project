
'use strict';

const ChildProcess = require('child_process');
const Colors = require('colors');
const GitSimple = require('simple-git');
const Path = require('path');
const Fs = require('fs');


class MakeRelease {
  constructor(app, releaseType, repoManager, fprintGitStatus) {
    this.app = app;
    this.releaseType = releaseType;
    this.repoManager = repoManager;
    this.currentVersion = '';
    this.newVersion = '';
    this.fprintGitStatus = fprintGitStatus;
    this.iteration = 0;
    this.verbose = Reflect.get(global, 'dynamicConfig@abstractor').parameters.has('verbose') ? true : false; 
  }
  
  execute(taskChain, cb) {
    this.repoManager.init(cb, () => {
      taskChain();
    });
  }
  
  iterate(parts, cbIter, cb) {
    const chosenParts = parts ? parts : MakeRelease.ALL
    let pendings = 0;
    const errors = [];
    const results = [];
    const path = this.repoManager.repos.build.path;
    this.repoManager.repos.build.repos.forEach((repo) => {
      if(this.app === repo.folder) {
        if(!repo.release) {
          cb(new Error('No release data for app: ', this.app), null);
        }
        let projectsToUpdate = MakeRelease.ALL === chosenParts || MakeRelease.APP === chosenParts ? [{
          repo: repo.folder,
          update: true
        }] : [];
        if(MakeRelease.ALL === chosenParts || MakeRelease.DEP === chosenParts) {
          repo.release.dependencies.forEach((dependency) => {
            projectsToUpdate.push(dependency);
          });
        }
        projectsToUpdate.forEach((projectToUpdate) => {
          ++pendings;
          if(projectToUpdate.update) {
            const project = this.repoManager.get(projectToUpdate.repo);
            cbIter(project, (err, result) => {
              if(err) {
                errors.push(err);
              }
              if(result) {
                results.push(result);
              }
              if(0 === --pendings) {
                cb(0 !== errors.length ? errors : undefined, 0 !== results.length ? results : undefined);
              }
            });
          }
          else {
            process.nextTick(() => {
              if(0 === --pendings) {
                cb(0 !== errors.length ? errors : undefined, 0 !== results.length ? results : undefined);
              }
            });  
          }
        });
      }
    });
  }
  
  calculateNewVersion(currentVersion) {
    const verstionShortName = currentVersion.split('-');
    if(1 !== verstionShortName.length && 2 !== verstionShortName.length) {
      console.log(`version is wrong formatted. '`.red + currentVersion + `'`);
    }
    const version = verstionShortName[0].split('.');
    if(3 !== version.length) {
      console.log(`version is wrong formatted. '`.red + currentVersion + `'`);
    }
    if('Major' === this.releaseType) {
      version[0] = "" + (Number.parseInt(version[0]) + 1);
      version[1] = '0';
      version[2] = '0';
    }
    else if('Minor' === this.releaseType) {
      version[1] = "" + (Number.parseInt(version[1]) + 1);
      version[2] = '0';
    }
    else if('Patch' === this.releaseType) {
      version[2] = "" + (Number.parseInt(version[2]) + 1);
    }
    this.currentVersion = currentVersion;
    this.newVersion = version.join('.');
  }
  
  getNewVersion() {
    return this.newVersion + '-' + this.shortName;
  }

  updateRepo(repo, repoName) {
    return !repo.data.release.dependencies ? false : -1 !== repo.data.release.dependencies.findIndex((dependency) => {
      return dependency.repo === repoName && dependency.update;
    });
  }
  
  startRelease(previousResult, cb) {
    console.log('   *************************************************'.yellow);
    console.log('   *************************************************'.yellow);
    console.log('   ******'.yellow, this.app, '-'.yellow, this.releaseType, '******'.yellow);
    console.log('   *************************************************'.yellow);
    console.log('   *************************************************'.yellow);
    process.nextTick(() => {
      cb();
    });  
  }
  
  startPublish(previousResult, cb) {
    console.log('   *************************************************'.yellow);
    console.log('   *************************************************'.yellow);
    console.log('   ******'.yellow, this.app, '-'.yellow, this.releaseType, '******'.yellow);
    console.log('   *************************************************'.yellow);
    console.log('   *************************************************'.yellow);
    process.nextTick(() => {
      cb();
    });  
  }
  
  readFileJson(file, cbRepoData) {
    Fs.readFile(file, (err, data) => {
      if(err) {
        const errText = `Could not read file: '${file}'`;
        console.log(errText);
        return cbRepoData(err);
      }
      let dataObject = null;
      try {
        dataObject = JSON.parse(data);
      }
      catch(err) {
        console.log(`Could not parse file: '${file}'`, err);
        return cbRepoData(err);
      }
      cbRepoData(undefined, dataObject);
    });
  }

  doesAllRepoPathsExist(previousResult, cb, parts) {
    console.log(`${++this.iteration}) Make sure repo paths exist`.yellow);
    this.iterate(parts, (repo, cbError) => {
      if(this.verbose) {
        console.log('     *'.cyan, repo.data.folder.cyan);
      }
      Fs.lstat(`${repo.path}${Path.sep}${repo.data.folder}`, (err, stat) => {
        if(err) {
          cbError({
            err: err,
            text: `Repo path '${repo.path}${Path.sep}${repo.data.folder}' does not exist.`
          }, `${repo.path}${Path.sep}${repo.data.folder}`);
        }
        else {
          cbError();
        }
      });
    }, (errors, results) => {
      if(errors) {
        console.log('   All repo paths does not exist'.yellow);
        errors.forEach((error) => {
          console.log('   ' + error.text);
        });
      }
      else {
        console.log('   All repo paths does exist'.green);
      }
      cb(errors, results);
    });
  }
  
  createNoneExistingRepoPaths(previousResult, cb, parts) {
    console.log(`${++this.iteration}) Create none exisitng repo paths`.yellow);
    this.iterate(parts, (repo, cbError) => {
      if(this.verbose) {
        console.log('     *'.cyan, repo.data.folder.cyan);
      }
      const found = previousResult.find((result) => {
        return result === `${repo.path}${Path.sep}${repo.data.folder}`
      });
      if(!found) {
        process.nextTick(() => {
          cbError();
        });
      }
      else {
        Fs.mkdir(`${repo.path}${Path.sep}${repo.data.folder}`, (err, stat) => {
          if(err) {
            cbError({
              err: err,
              text: `Could not create repo path '${repo.path}${Path.sep}${repo.data.folder}'`
            });
          }
          else {
            cbError(undefined, `Repo path '${repo.path}${Path.sep}${repo.data.folder}' is created.`);
          }
        });
      }
    }, (errors, results) => {
      if(errors) {
        console.log('   Could not create all none existing repo paths'.red);
        results.forEach((result) => {
          console.log('   ' + result.text);
        });
        errors.forEach((error) => {
          console.log('   ' + error.text);
        });
      }
      else {
        console.log('   All none existing repo paths are created'.green);
        results.forEach((result) => {
          console.log('   ' + result);
        });
      }
      cb(errors, results);
    });
  }
  
  doesAllRepoPathsContainARepo(previousResult, cb, parts) {
    console.log(`${++this.iteration}) Make sure repo paths contain a repo`.yellow);
    this.iterate(parts, (repo, cbError) => {
      if(this.verbose) {
        console.log('     *'.cyan, repo.data.folder.cyan);
      }
      GitSimple(`${repo.path}${Path.sep}${repo.data.folder}`).checkIsRepo((err, result) => {
        if(err || !result) {
          cbError({
            err: err,
            text: `Repo path '${repo.path}${Path.sep}${repo.data.folder}' does not contain a repo.`
          }, repo.data.folder);
        }
        else {
          cbError();
        }
      });
    }, (errors, results) => {
      if(errors) {
        console.log('   All repo paths does not contain a repo'.yellow);
        errors.forEach((error) => {
          console.log('   ' + error.text);
        });
      }
      else {
        console.log('   All repo paths contain a repo'.green);
      }
      cb(errors, results);
    });
  }
  
  cloneNoneExistingRepo(previousResult, cb, parts) {
    console.log(`${++this.iteration}) clone none exisitng repos`.yellow);
    this.iterate(parts, (repo, cbError) => {
      if(this.verbose) {
        console.log('     *'.cyan, repo.data.folder.cyan);
      }
      const found = previousResult.find((result) => {
        return result === repo.data.folder;
      });
      if(!found) {
        process.nextTick(() => {
          cbError();
        });
      }
      else {
        const repoData = this.repoManager.get(found);
        GitSimple().clone(repo.data.repo, `${repo.path}${Path.sep}${repo.data.folder}`, (err, result) => {
          if(err) {
            cbError({
              err: err,
              text: `Could not clone repo '${repo.data.folder}' @ ${repo.data.repo}`
            });
          }
          else {
            cbError(undefined, `Repo '${repo.data.folder}' @ ${repo.data.repo} is cloned.`);
          }
        });
      }
    }, (errors, results) => {
      if(errors) {
        console.log('   Could not clone all none existing repos'.red);
        if(results) {
          results.forEach((result) => {
            console.log('   ' + result.text);
          });
        }
        errors.forEach((error) => {
          console.log('   ' + error.text);
        });
      }
      else {
        console.log('   All none existing repos are cloned'.green);
        results.forEach((result) => {
          console.log('   ' + result);
        });
      }
      cb(errors, results);
    });
  }
  
  gitStatus(previousResult, cb, parts) {
    console.log(`${++this.iteration}) check git status on the repos`.yellow);
    this.iterate(parts, (repo, cbError) => {
      if(this.verbose) {
        console.log('     *'.cyan, repo.data.folder.cyan);
      }
      GitSimple(Path.normalize(`${repo.path}${Path.sep}${repo.data.folder}`)).status((err, result) => {
        const ready = 0 === result.not_added.length && 0 === result.conflicted.length && 0 === result.created.length && 0 === result.deleted.length && 0 === result.modified.length && 0 === result.renamed.length && 0 === result.staged.length && 0 === result.ahead && 0 == result.behind;
        if(err || !ready) {
            cbError({
              err: err,
              text: 'Repo'.red + ` '${repo.data.folder}' @ ${repo.data.repo} ` + 'is not ready for release.'.red
            }, {
              name: repo.data.folder,
              path: `${repo.path}${Path.sep}${repo.data.folder}`,
              error: err,
              isFolder: true,
              isRepo: true,
              ready: false,
              result: result
            });
          }
          cbError();
        });
    }, (errors, results) => {
      if(errors) {
        console.log('   All repos are not ready for realease.'.red);
        errors.forEach((error) => {
          console.log('   ' + error.text);
        });
        console.log('   *************************************************'.yellow);
        console.log('   *** git status on repos '.yellow + 'not'.red + ' ready for release ***'.yellow);
        console.log('   *************************************************'.yellow);
        results.forEach((result) => {
          this.fprintGitStatus(result);
        });
      }
      else {
        console.log('   All repos are ready for release.'.green);
      }
      cb(errors, results);
    });
  }
  
  readPackageJson(previousResult, cb, parts) {
    console.log(`${++this.iteration}) Read package.json files.`.yellow);
    this.iterate(parts, (repo, cbError) => {
      if(this.verbose) {
        console.log('     *'.cyan, repo.data.folder.cyan);
      }
      this.readFileJson(Path.normalize(`${repo.path}${Path.sep}${repo.data.folder}${Path.sep}package.json`), (err, packageJson) => {
        if(err) {
          cbError({
            err: err,
            text: `Could not read package.json from repo '${repo.data.folder}' @ ${repo.data.repo}`
          });
        }
        else {
          cbError(undefined, {
            packageJson, packageJson,
            repoName: repo.data.folder
          });
        }
      });
    }, (errors, results) => {
      if(errors) {
        console.log('   Could not read all package.json files.'.red);
        errors.forEach((error) => {
          console.log('   ' + error.text);
        });
      }
      else {
        console.log('   All package.json files are read.'.green);
        results.forEach((result) => {
          this.repoManager.get(result.repoName).packageJson = result.packageJson;
          if(this.app === result.repoName) {
            this.appName = result.packageJson.name;
            this.shortName = result.packageJson.shortname;
            this.calculateNewVersion(result.packageJson.version);
          }
        });
      }
      cb(errors, results);
    });
  }
  
  updateVersionInPackageJson(previousResult, cb, parts) {
    console.log(`${++this.iteration}) Update version in package.json files.`.yellow);
    this.iterate(parts, (repo, cbError) => {
      if(this.verbose) {
        console.log('     *'.cyan, repo.data.folder.cyan);
      }
      process.nextTick(() => {
        const repoData = this.repoManager.get(repo.data.folder);
        repoData.packageJson.version = this.getNewVersion();
        cbError();
      });
    }, (errors, results) => {
      if(errors) {
        console.log('   All package.json files could not be updated with the new version.'.red);
        errors.forEach((error) => {
          console.log('   ' + error.text);
        });
      }
      else {
        console.log('   All package.json files are updated with the new version:'.green, this.getNewVersion() + ', previous version:'.yellow, this.currentVersion);

      }
      cb(errors, results);
    });
  }
  
  writePackageJson(previousResult, cb, parts) {
    console.log(`${++this.iteration}) Write package.json files.`.yellow);
    this.iterate(parts, (repo, cbError) => {
      if(this.verbose) {
        console.log('     *'.cyan, repo.data.folder.cyan);
      }
      const repoData = this.repoManager.get(repo.data.folder);
      Fs.writeFile(Path.normalize(`${repo.path}${Path.sep}${repo.data.folder}${Path.sep}package.json`), JSON.stringify(repoData.packageJson, null, 2) + '\r\n', (err) => {
        if(err) {
          cbError({
            err: err,
            text: `Could not write package.json from repo '${repo.data.folder}' @ ${repo.data.repo}`
          });
        }
        else {
          cbError();
        }
      });
    }, (errors, results) => {
      if(errors) {
        console.log('   Could not write all package.json files.'.red);
        errors.forEach((error) => {
          console.log('   ' + error.text);
        });
      }
      else {
        console.log('   All package.json files are written.'.green);
      }
      cb(errors, results);
    });
  }
  
  gitAddPackageJson(previousResult, cb, parts) {
    console.log(`${++this.iteration}) Git add package.json files.`.yellow);
    this.iterate(parts, (repo, cbError) => {
      if(this.verbose) {
        console.log('     *'.cyan, repo.data.folder.cyan);
      }
      GitSimple(Path.normalize(`${repo.path}${Path.sep}${repo.data.folder}`)).add('package.json', (err) => {
        if(err) {
          cbError({
            err: err,
            text: `Could not git add package.json from repo '${repo.data.folder}' @ ${repo.data.repo} to git`
          });
        }
        else {
          cbError();
        }
      });
    }, (errors, results) => {
      if(errors) {
        console.log('   All package.json files could not be added to git.'.red);
        errors.forEach((error) => {
          console.log('   ' + error.text);
        });
      }
      else {
        console.log('   All package.json files are added to git'.green);
      }
      cb(errors, results);
    });
  }
  
  gitCommitPackageJson(previousResult, cb, parts) {
    console.log(`${++this.iteration}) Git commit package.json files.`.yellow);
    this.iterate(parts, (repo, cbError) => {
      if(this.verbose) {
        console.log('     *'.cyan, repo.data.folder.cyan);
      }
      GitSimple(Path.normalize(`${repo.path}${Path.sep}${repo.data.folder}`)).commit(`Updated package.json for version ${this.getNewVersion()}`, (err) => {
        if(err) {
          cbError({
            err: err,
            text: `Could not git commit package.json from repo '${repo.data.folder}' @ ${repo.data.repo} to git`
          });
        }
        else {
          cbError();
        }
      });
    }, (errors, results) => {
      if(errors) {
        console.log('   All package.json files could not be commited to git.'.red);
        errors.forEach((error) => {
          console.log('   ' + error.text);
        });
      }
      else {
        console.log('   All package.json files are commited to git'.green);
      }
      cb(errors, results);
    });
  }
  
  npmInstallRepos(previousResult, cb, parts) {
    console.log(`${++this.iteration}) npm install to update package-lock.json.`.yellow);  
    this.iterate(parts, (repo, cbError) => {
      const folder = Path.normalize(`${repo.path}${Path.sep}${repo.data.folder}`);
      if(this.verbose) {
        console.log('     *'.cyan, repo.data.folder.cyan, 'npm install --package-lock-only', '@'.cyan, folder);
      }
      try {
        ChildProcess.exec('npm install --package-lock-only', {cwd: folder}, (err, stdout, stderr) => {
          if(err) {
            cbError({
              err: err,
              text: `npm install failure on '${repo.data.folder}' @ ${repo.data.repo}`
            });
          }
          else {
            cbError();
          }
        });
      } catch (err) {
        cbError({
          err: err,
          text: `npm install failure on '${repo.data.folder}' @ ${repo.data.repo}, err`
        });
      }
    }, (errors, results) => {
      if(errors) {
        console.log('   There where failures on'.red, 'npm install'.yellow + '.'.red);
        errors.forEach((error) => {
          console.log('   ' + error.text);
        });
      }
      else {
        console.log('   All'.green, 'npm install'.yellow, 'succeeded'.green);
      }
      cb(errors, results);
    });
  }
  
  gitAddPackageLockJson(previousResult, cb, parts) {
    console.log(`${++this.iteration}) Git add package.json files.`.yellow);
    this.iterate(parts, (repo, cbError) => {
      if(this.verbose) {
        console.log('     *'.cyan, repo.data.folder.cyan);
      }
      GitSimple(Path.normalize(`${repo.path}${Path.sep}${repo.data.folder}`)).add('./*', (err) => {
        if(err) {
          cbError({
            err: err,
            text: `Could not git add package.json from repo '${repo.data.folder}' @ ${repo.data.repo} to git`
          });
        }
        else {
          cbError();
        }
      });
    }, (errors, results) => {
      if(errors) {
        console.log('   All package.json files could not be added to git.'.red);
        errors.forEach((error) => {
          console.log('   ' + error.text);
        });
      }
      else {
        console.log('   All package.json files are added to git'.green);
      }
      cb(errors, results);
    });
  }
  
  gitCommitPackageLockJson(previousResult, cb, parts) {
    console.log(`${++this.iteration}) Git commit package.json files.`.yellow);
    this.iterate(parts, (repo, cbError) => {
      if(this.verbose) {
        console.log('     *'.cyan, repo.data.folder.cyan);
      }
      GitSimple(Path.normalize(`${repo.path}${Path.sep}${repo.data.folder}`)).commit(`Abstraktor AB - ${this.appName} - Release version ${this.getNewVersion()}`, (err) => {
        if(err) {
          cbError({
            err: err,
            text: `Could not git commit package.json from repo '${repo.data.folder}' @ ${repo.data.repo} to git`
          });
        }
        else {
          cbError();
        }
      });
    }, (errors, results) => {
      if(errors) {
        console.log('   All package.json files could not be commited to git.'.red);
        errors.forEach((error) => {
          console.log('   ' + error.text);
        });
      }
      else {
        console.log('   All package.json files are commited to git'.green);
      }
      cb(errors, results);
    });
  }
  
  gitPushPackageJson(previousResult, cb, parts) {
    console.log(`${++this.iteration}) Git push package.json files.`.yellow);
    this.iterate(parts, (repo, cbError) => {
      if(this.verbose) {
        console.log('     *'.cyan, repo.data.folder.cyan);
      }
      GitSimple(Path.normalize(`${repo.path}${Path.sep}${repo.data.folder}`)).push((err) => {
        if(err) {
          cbError({
            err: err,
            text: `Could not git push package.json from repo '${repo.data.folder}' @ ${repo.data.repo} to git`
          });
        }
        else {
          cbError();
        }
      });
    }, (errors, results) => {
      if(errors) {
        console.log('   All package.json files could not be pushed to git.'.red);
        errors.forEach((error) => {
          console.log('   ' + error.text);
        });
      }
      else {
        console.log('   All package.json files are pushed to git'.green);
      }
      cb(errors, results);
    });
  }
  
  gitTagPackageJson(previousResult, cb, parts) {
    console.log(`${++this.iteration}) Git tag repos.`.yellow);
    this.iterate(parts, (repo, cbError) => {
      if(this.verbose) {
        console.log('     *'.cyan, repo.data.folder.cyan);
      }
      GitSimple(Path.normalize(`${repo.path}${Path.sep}${repo.data.folder}`)).addAnnotatedTag(`v${this.getNewVersion()}`, `Abstraktor AB - ${this.appName} - Internal Release version ${this.getNewVersion()}`, (err, tagResult) => {
        if(err) {
          cbError({
            err: err,
            text: `Could not git push package.json from repo '${repo.data.folder}' @ ${repo.data.repo} to git`
          });
        }
        else {
          cbError();
        }
      });
    }, (errors, results) => {
      if(errors) {
        console.log('   All repos could not be tagged in git.'.red);
        errors.forEach((error) => {
          console.log('   ' + error.text);
        });
      }
      else {
        console.log('   All repos are tagged in git'.green);
      }
      cb(errors, results);
    });
  }
  
  gitPushTagPackageJson(previousResult, cb, parts) {
    console.log(`${++this.iteration}) Git push tag repos.`.yellow);
    this.iterate(parts, (repo, cbError) => {
      if(this.verbose) {
        console.log('     *'.cyan, repo.data.folder.cyan);
      }
      GitSimple(Path.normalize(`${repo.path}${Path.sep}${repo.data.folder}`)).pushTags((err, tagResult) => {
        if(err) {
          cbError({
            err: err,
            text: `Could not git push package.json from repo '${repo.data.folder}' @ ${repo.data.repo} to git`
          });
        }
        else {
          cbError();
        }
      });
    }, (errors, results) => {
      if(errors) {
        console.log('   All repos tags could not be pushed in git.'.red);
        errors.forEach((error) => {
          console.log('   ' + error.text);
        });
      }
      else {
        console.log('   All repos are tags are pushed in git'.green);
      }
      cb(errors, results);
    });
  }
  
  publishFolderExists(previousResult, cb) {
    console.log(`${++this.iteration}) Is the publish folder already created.`.yellow);
    const folder = `..${Path.sep}Publish${Path.sep}${this.app}#v${this.getNewVersion()}`;
    Fs.lstat(folder, (err, stat) => {
      if(!err) {
        console.log('   The publish folder \''.red + folder + '\' already exist.'.red);
        cb(new Error('exist'));
      }
      else {
        console.log('   The publish \''.green + folder + '\' do not exist.'.green);
        cb();
      }
    });
  }
  
  createPublishFolder(previousResult, cb) {
    const folder = `..${Path.sep}Publish${Path.sep}${this.app}#v${this.getNewVersion()}`;
    console.log(`${++this.iteration}) Create publish folder.`.yellow, folder);
    Fs.mkdir(Path.normalize(folder), { recursive: true }, (err) => {
      if(err) {
        console.log('   Could not create the publish folder \''.red + folder + '\'.'.red);
      }
      else {
        console.log('   The publish folder \''.green + folder + '\' is created.'.green);
      }
      cb(err);
    });
  }
  
  clonePublishRepos(previousResult, cb, parts) {
    console.log(`${++this.iteration}) Clone publish repos`.yellow);
    this.iterate(parts, (repo, cbError) => {
      if(this.verbose) {
        console.log('     *'.cyan, repo.data.folder.cyan);
      }
      const repoData = this.repoManager.get(repo.data.folder);
      const folder = Path.normalize(`..${Path.sep}Publish${Path.sep}${this.app}#v${this.getNewVersion()}${Path.sep}vfe${Path.sep}${repo.path}${Path.sep}${repo.data.folder}`);
      GitSimple().clone(repo.data.release.repo, folder, (err, result) => {
        if(err) {
          cbError({
            err: err,
            text: `Could not clone repo '`.red + repo.data.folder + `' @ `.red + repo.data.release.repo
          });
        }
        else {
          cbError(undefined, `Repo '`.green + `${repo.data.folder}` + `' @ `.green + repo.data.release.repo + ' is cloned.'.green);
        }
      });
    }, (errors, results) => {
      if(errors) {
        console.log('   Could not clone all publish repos'.red);
        if(results) {
          results.forEach((result) => {
            console.log('   ' + result.text);
          });
        }
        errors.forEach((error) => {
          console.log('   ' + error.text, error.err);
        });
      }
      else {
        console.log('   All publish repos are cloned'.green);
        results.forEach((result) => {
          console.log('   ' + result);
        });
      }
      cb(errors, results);
    });
  }
  
  findFilesFromPublishRepo(previousResult, cb, parts) {
    console.log(`${++this.iteration}) Find all files in publish repos`.yellow);
    this.iterate(parts, (repo, cbError) => {
      if(this.verbose) {
        console.log('     *'.cyan, repo.data.folder.cyan);
      }
      const repoData = this.repoManager.get(repo.data.folder);
      const folder = Path.normalize(`..${Path.sep}Publish${Path.sep}${this.app}#v${this.getNewVersion()}${Path.sep}vfe${Path.sep}${repo.path}${Path.sep}${repo.data.folder}`);
      Fs.readdir(folder, (err, files) => {
        if(err) {
          cbError({
            err: err,
            text: 'Could not find all files in publish repos.'.red
          });
        }
        else {
          const index = files.indexOf('.git');
          if(-1 !== index) {
            files.splice(index, 1);
          }
          cbError(undefined, {
            repo: repo.data.folder,
            folder: folder,
            files: files
          });
        }
      });
    }, (errors, results) => {
      if(errors) {
        console.log('   Could not find all files in publish repos'.red);
        if(results) {
          results.forEach((result) => {
            console.log('   ' + result.text);
          });
        }
        errors.forEach((error) => {
          console.log('   ' + error.text, error.err);
        });
      }
      else {
        console.log('   All files publish repos are found'.green);
      }
      cb(errors, results);
    });
  }
  
  removeFilesFromPublishRepo(previousResult, cb, parts) {
    console.log(`${++this.iteration}) Remove all files and folders except .git from publish repos`.yellow);
    this.iterate(parts, (repo, cbError) => {
      if(this.verbose) {
        console.log('     *'.cyan, repo.data.folder.cyan);
      }
      const found = previousResult.find((result) => {
        return result.repo === repo.data.folder;
      });
      if(0 === found.files.length) {
        process.nextTick(() => {
          cbError();
        });
      }
      else {
        let pendings = 0; 
        found.files.forEach((file) => {
          ++pendings;
          const rmErrors = [];
          Fs.rm(`${found.folder}${Path.sep}${file}`, { recursive: true}, (err) => {
            if(err) {
              rmErrors.push(`Could not remove file '`.red + file + `'.`.red, err);
            }
            if(0 === --pendings) {
              if(0 === rmErrors.length) {
                cbError();
              }
              else {
                cbError({
                  err: err,
                  text: rmErrors.join('\r\n')
                });
              }
            }
          });
        });
      }
    }, (errors, results) => {
      if(errors) {
        console.log('   Could not remove all files and folders except .git from publish repo'.red);
        errors.forEach((error) => {
          console.log('   ' + error.text);
        });
      }
      else {
        console.log('   All files and folders except .git are removed from publish repo.'.green);
        if(results) {
          results.forEach((result) => {
            console.log('   ' + result);
          });
        }
      }
      cb(errors, results);
    });
  }

  copyRepoFolder(src, dst, cb) {
    let error = false;
    Fs.readdir(src, (err, files) => {
      let pendings = 0;
      if(err) {
        console.log(`Could not copy folder '`.red + `${src}` + `'.`.red);
        cb(err);
      }
      if(0 === files.length) {
        return cb();
      }
      files.forEach((file) => {
        ++pendings;
        Fs.stat(`${src}${Path.sep}${file}`, (err, stat) => {
          if(!err) {
            if(stat.isDirectory()) {
              if('.git' !== file && 'build' !== file && 'dist' !== file && 'node_modules' !== file) {
                ++pendings;
                Fs.mkdir(`${dst}${Path.sep}${file}`, (err) => {
                  this.copyRepoFolder(`${src}${Path.sep}${file}`, `${dst}${Path.sep}${file}`, (err) => {
                    if(err) {
                      error = true;
                      console.log(`Could not copy folder '`.red + `${src}${Path.sep}${file}` + `'.`.red);
                    }
                    if(0 === --pendings) {
                      cb(error ? new Error('Copy error') : undefined);
                    }
                  });
                });
              }
            }
            else if(stat.isFile()) {
              if('package-lock.json' !== file) {
                ++pendings;
                Fs.copyFile(`${src}${Path.sep}${file}`, `${dst}${Path.sep}${file}`, (err) => {
                  if(err) {
                    error = true;
                    console.log(`Could not copy file '`.red + `${src}${Path.sep}${file}` + `'.`.red);
                  }
                  if(0 === --pendings) {
                    cb(error ? new Error('Copy error') : undefined);
                  }
                });
              }
            }
          }
          else {
            error = true;
            console.log(err);
          }
          if(0 === --pendings) {
            cb(error ? new Error('Copy error') : undefined);
          }
        });
      });
    });
  }
  
  copyFilesToPublishRepo(previousResult, cb, parts) {
    console.log(`${++this.iteration}) Copy all files and folders except .git|build|dest|node_modules to publish repos`.yellow);
    this.iterate(parts, (repo, cbError) => {
      if(this.verbose) {
        console.log('     *'.cyan, repo.data.folder.cyan);
      }
      const src = Path.normalize(`${repo.path}${Path.sep}${repo.data.folder}`);
      const dst = Path.normalize(`..${Path.sep}Publish${Path.sep}${this.app}#v${this.getNewVersion()}${Path.sep}vfe${Path.sep}${repo.path}${Path.sep}${repo.data.folder}`);
      this.copyRepoFolder(src, dst, cbError);
    }, (errors, results) => {
      if(errors) {
        console.log('   Could not remove all files and folders except .git from publish repo'.red);
        errors.forEach((error) => {
          console.log('   ' + error.text);
        });
      }
      else {
        console.log('   All files and folders except .git are removed from publish repo.'.green);
        if(results) {
          results.forEach((result) => {
            console.log('   ' + result);
          });
        }
      }
      cb(errors, results);
    });
  }
  
  createPublishPackageJsonFromReleasePackageJson(previousResult, cb, parts) {
    console.log(`${++this.iteration}) Create publish package.json files from package_release.json.`.yellow);
    this.iterate(parts, (repo, cbError) => {
      if(repo.data.release.releasePackageJson) {
        if(this.verbose) {
          console.log('     *'.cyan, repo.data.folder.cyan);
          const folder = Path.normalize(`..${Path.sep}Publish${Path.sep}${this.app}#v${this.getNewVersion()}${Path.sep}vfe${Path.sep}${repo.path}${Path.sep}${repo.data.folder}${Path.sep}package.json`);
          console.log(folder);
          Fs.rmdir(folder, {
            recursive: true
          }, (err) => {
            if(err) {
              cbError({
                err: err,
                text: `Could not remove publish package.json from repo '${repo.data.folder}' @ ${repo.data.release.repo}`
              });
            }
            else {
              const releaseFolder = Path.normalize(`..${Path.sep}Publish${Path.sep}${this.app}#v${this.getNewVersion()}${Path.sep}vfe${Path.sep}${repo.path}${Path.sep}${repo.data.folder}${Path.sep}package_release.json`);
              Fs.rename(releaseFolder, folder, (err) => {
                if(err) {
                  cbError({
                    err: err,
                    text: `Could not rename publish package_release.json to package.json from repo '${repo.data.folder}' @ ${repo.data.release.repo}`
                  });
                }
                else {
                  cbError();
                }
              });
            }
          });
        }
      }
      else {
        process.nextTick(() => {
          cbError();
        });
      }
    }, (errors, results) => {
      cb(errors, results);
    });
  }
  
  readPublishPackageJson(previousResult, cb, parts) {
    console.log(`${++this.iteration}) Read publish package.json files.`.yellow);
    this.iterate(parts, (repo, cbError) => {
      if(this.verbose) {
        console.log('     *'.cyan, repo.data.folder.cyan);
      }      
      const folder = Path.normalize(`..${Path.sep}Publish${Path.sep}${this.app}#v${this.getNewVersion()}${Path.sep}vfe${Path.sep}${repo.path}${Path.sep}${repo.data.folder}${Path.sep}package.json`);
      this.readFileJson(folder, (err, packageJson) => {
        if(err) {
          cbError({
            err: err,
            text: `Could not read publish package.json from repo '${repo.data.folder}' @ ${repo.data.release.repo}`
          });
        }
        else {
          cbError(undefined, {
            packageJson, packageJson,
            repoName: repo.data.folder
          });
        }
      });
    }, (errors, results) => {
      if(errors) {
        console.log('   Could not read all publish  package.json files.'.red);
        errors.forEach((error) => {
          console.log('   ' + error.text);
        });
      }
      else {
        console.log('   All publish  package.json files are read.'.green);
        results.forEach((result) => {
          this.repoManager.get(result.repoName).packageJson = result.packageJson;
        });
      }
      cb(errors, results);
    });
  }
  
  updatePublishPackageJson(previousResult, cb, parts) {
    console.log(`${++this.iteration}) Update publish package.json files.`.yellow);
    this.iterate(parts, (repo, cbError) => {
      const folder = Path.normalize(`..${Path.sep}Publish${Path.sep}${this.app}#v${this.getNewVersion()}${Path.sep}vfe${Path.sep}${repo.path}${Path.sep}${repo.data.folder}${Path.sep}package.json`);
      if(this.verbose) {
        console.log('     *'.cyan, repo.data.folder.cyan, 'update package.json'.cyan, folder);
        console.log('       *'.cyan, repo.data.folder.cyan, 'repository: '.cyan, repo.data.release.repo);
      }
      this.repoManager.get(repo.data.folder).packageJson.repository = repo.data.release.repo;
	  this.repoManager.get(repo.data.folder).packageJson.version = this.getNewVersion();
      process.nextTick(() => {
        cbError();
      });
    }, (errors, results) => {
      if(errors) {
        console.log('   Could not update all publish  package.json files.'.red);
        errors.forEach((error) => {
          console.log('   ' + error.text);
        });
      }
      else {
        console.log('   All publish  package.json files are updated.'.green);
      }
      cb(errors, results);
    });
  }
  
  updateAbstractorDependenciesInPublishPackageJson(previousResult, cb, parts) {
    console.log(`${++this.iteration}) Update Abstraktor dependencies in publish package.json files.`.yellow);
    this.iterate(parts, (repo, cbError) => {
      process.nextTick(() => {
        if(this.verbose) {
          console.log('     *'.cyan, repo.data.folder.cyan);
        }
        const repoData = this.repoManager.get(repo.data.folder);
        if(repoData.packageJson.dependencies) {
          const dependencyKeys = Reflect.ownKeys(repoData.packageJson.dependencies);
          dependencyKeys.forEach((dependencyKey) => {
            if(dependencyKey.startsWith('z-abs-')) {
              const update = this.updateRepo(repo, dependencyKey);
              const dependecyValue = Reflect.get(repoData.packageJson.dependencies, dependencyKey);
              const repoDataDependency = this.repoManager.get(dependencyKey);
              const newDependency = `git+${repoDataDependency.data.release.repo}${update ? '#v' + this.getNewVersion() :''}`;
              Reflect.set(repoData.packageJson.dependencies, dependencyKey, newDependency);
            }
          });
        }
        cbError();
      });
    }, (errors, results) => {
      if(errors) {
        console.log('   All publish  package.json files could not be updated with the new version.'.red);
        errors.forEach((error) => {
          console.log('   ' + error.text);
        });
      }
      else {
        console.log('   All publish package.json files are updated with the new version:'.green, this.getNewVersion() + '.'.yellow);
      }
      cb(errors, results);
    });
  }
  
  writePublishPackageJson(previousResult, cb, parts) {
    console.log(`${++this.iteration}) Write publish package.json files.`.yellow);
    this.iterate(parts, (repo, cbError) => {
      if(this.verbose) {
        console.log('     *'.cyan, repo.data.folder.cyan);
      }
      const repoData = this.repoManager.get(repo.data.folder);
      const folder = Path.normalize(`..${Path.sep}Publish${Path.sep}${this.app}#v${this.getNewVersion()}${Path.sep}vfe${Path.sep}${repo.path}${Path.sep}${repo.data.folder}${Path.sep}package.json`);
      Fs.writeFile(folder, JSON.stringify(repoData.packageJson, null, 2) + '\r\n', (err) => {
        if(err) {
          cbError({
            err: err,
            text: `Could not write publish package.json from repo '${repo.data.folder}' @ ${repo.data.release.repo}`
          });
        }
        else {
          cbError();
        }
      });
    }, (errors, results) => {
      if(errors) {
        console.log('   Could not write all publish package.json files.'.red);
        errors.forEach((error) => {
          console.log('   ' + error.text);
        });
      }
      else {
        console.log('   All publish package.json files are written.'.green);
      }
      cb(errors, results);
    });
  }
  
  gitAddPublish(previousResult, cb, parts) {
    console.log(`${++this.iteration}) Git add publish files.`.yellow);
    this.iterate(parts, (repo, cbError) => {
      if(this.verbose) {
        console.log('     *'.cyan, repo.data.folder.cyan);
      }
      const folder = Path.normalize(`..${Path.sep}Publish${Path.sep}${this.app}#v${this.getNewVersion()}${Path.sep}vfe${Path.sep}${repo.path}${Path.sep}${repo.data.folder}`);
      GitSimple(folder).add('./*', (err) => {
        if(err) {
          cbError({
            err: err,
            text: `Could not git add publish files '${repo.data.folder}' @ ${repo.data.release.repo} to git`
          });
        }
        else {
          cbError();
        }
      });
    }, (errors, results) => {
      if(errors) {
        console.log('   All publish files could not be added to git.'.red);
        errors.forEach((error) => {
          console.log('   ' + error.text);
        });
      }
      else {
        console.log('   All publish files are added to git'.green);
      }
      cb(errors, results);
    });
  }
  
  gitCommitPublish(previousResult, cb, parts) {
    console.log(`${++this.iteration}) Git commit publish files.`.yellow);
    this.iterate(parts, (repo, cbError) => {
      if(this.verbose) {
        console.log('     *'.cyan, repo.data.folder.cyan);
      }
      const folder = Path.normalize(`..${Path.sep}Publish${Path.sep}${this.app}#v${this.getNewVersion()}${Path.sep}vfe${Path.sep}${repo.path}${Path.sep}${repo.data.folder}`);
      GitSimple(folder).commit(`Abstraktor AB - ${this.appName} - Release version ${this.getNewVersion()}`, (err) => {
        if(err) {
          cbError({
            err: err,
            text: `Could not git commit publish files '${repo.data.folder}' @ ${repo.data.release.repo} to git`
          });
        }
        else {
          cbError();
        }
      });
    }, (errors, results) => {
      if(errors) {
        console.log('   All publish files could not be commited to git.'.red);
        errors.forEach((error) => {
          console.log('   ' + error.text);
        });
      }
      else {
        console.log('   All publish files are commited to git'.green);
      }
      cb(errors, results);
    });
  }
  
  npmInstallPublishRepos(previousResult, cb, parts) {
    console.log(`${++this.iteration}) npm install to update package-lock.json.`.yellow);  
    this.iterate(parts, (repo, cbError) => {
      const folder = Path.normalize(`..${Path.sep}Publish${Path.sep}${this.app}#v${this.getNewVersion()}${Path.sep}vfe${Path.sep}${repo.path}${Path.sep}${repo.data.folder}`);
      if(this.verbose) {
        console.log('     *'.cyan, repo.data.folder.cyan, 'npm install  --package-lock-only', '@'.cyan, folder);
      }
      try {
        ChildProcess.exec('npm install  --package-lock-only', {cwd: folder}, (err, stdout, stderr) => {
          if(this.verbose) {
            console.log(stdout);
          }
          console.log(stderr);
          if(err) {
            cbError({
              err: err,
              text: `npm install failure on '${repo.data.folder}' @ ${repo.data.release.repo}`, err
            });
          }
          else {
            cbError();
          }
        });
      } catch (err) {
        cbError({
          err: err,
          text: `npm install failure on '${repo.data.folder}' @ ${repo.data.release.repo}`, err
        });
      }
    }, (errors, results) => {
      if(errors) {
        console.log('   There where failures on'.red, 'npm install'.yellow + '.'.red);
        errors.forEach((error) => {
          console.log('   ' + error.text);
        });
      }
      else {
        console.log('   All'.green + 'npm install'.yellow, 'succeeded'.green);
      }
      cb(errors, results);
    });
  }
  
  gitPushPublish(previousResult, cb, parts) {
    console.log(`${++this.iteration}) Git push publish files.`.yellow);
    this.iterate(parts, (repo, cbError) => {
      if(this.verbose) {
        console.log('     *'.cyan, repo.data.folder.cyan);
      }
      const folder = Path.normalize(`..${Path.sep}Publish${Path.sep}${this.app}#v${this.getNewVersion()}${Path.sep}vfe${Path.sep}${repo.path}${Path.sep}${repo.data.folder}`);
      GitSimple(folder).push((err) => {
        if(err) {
          cbError({
            err: err,
            text: `Could not git push publish files '${repo.data.folder}' @ ${repo.data.release.repo} to git`
          });
        }
        else {
          cbError();
        }
      });
    }, (errors, results) => {
      if(errors) {
        console.log('   All publish files could not be pushed to git.'.red);
        errors.forEach((error) => {
          console.log('   ' + error.text);
        });
      }
      else {
        console.log('   All publish files are pushed to git'.green);
      }
      cb(errors, results);
    });
  }
  
  gitTagPublish(previousResult, cb, parts) {
    console.log(`${++this.iteration}) Git tag publish repos.`.yellow);
    this.iterate(parts, (repo, cbError) => {
      if(this.verbose) {
        console.log('     *'.cyan, repo.data.folder.cyan);
      }
      const folder = Path.normalize(`..${Path.sep}Publish${Path.sep}${this.app}#v${this.getNewVersion()}${Path.sep}vfe${Path.sep}${repo.path}${Path.sep}${repo.data.folder}`);
      GitSimple(folder).addAnnotatedTag(`v${this.getNewVersion()}`, `Abstraktor AB - ${this.appName} - Release version ${this.getNewVersion()}`, (err, tagResult) => {
        if(err) {
          cbError({
            err: err,
            text: `Could not tag publish repos '${repo.data.folder}' @ ${repo.data.release.repo}`
          });
        }
        else {
          cbError();
        }
      });
    }, (errors, results) => {
      if(errors) {
        console.log('   All publish repos could not be tagged.'.red);
        errors.forEach((error) => {
          console.log('   ' + error.text, error.err);
        });
      }
      else {
        console.log('   All repos are tagged'.green);
      }
      cb(errors, results);
    });
  }
  
  gitPushTagPublish(previousResult, cb, parts) {
    console.log(`${++this.iteration}) Git push tagged publish repos.`.yellow);
    this.iterate(parts, (repo, cbError) => {
      if(this.verbose) {
        console.log('     *'.cyan, repo.data.folder.cyan);
      }
      const folder = Path.normalize(`..${Path.sep}Publish${Path.sep}${this.app}#v${this.getNewVersion()}${Path.sep}vfe${Path.sep}${repo.path}${Path.sep}${repo.data.folder}`);
      GitSimple(folder).pushTags((err, tagResult) => {
        if(err) {
          cbError({
            err: err,
            text: `Could not push all publish repos'${repo.data.folder}' @ ${repo.data.release.repo}`
          });
        }
        else {
          cbError();
        }
      });
    }, (errors, results) => {
      if(errors) {
        console.log('   All publish repos tags could not be pushed.'.red);
        errors.forEach((error) => {
          console.log('   ' + error.text);
        });
      }
      else {
        console.log('   All publish repo tags are pushed'.green);
      }
      cb(errors, results);
    });
  }
}

MakeRelease.ALL = 0;
MakeRelease.APP = 1;
MakeRelease.DEP = 2;

module.exports = MakeRelease;
