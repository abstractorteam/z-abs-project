
'use strict';

class TaskChain {
  constructor(handler, nextHandler, object, cb, first = true, ...params) {
    this.handler = handler.bind(object);
    this.nextHandler = nextHandler
    this.params = params;
    this.cb = cb;
    this.object = object;
    this.next = null;
    if(first) {
      process.nextTick(() => {
        this.then();
      });
    }
  }
  
  pipe(handler, nextHandler, ...params) {
    return this.next = new TaskChain(handler, nextHandler, this.object, this.cb, false, ...params);
  }
  
  then(previousResults) {
    if(this.handler) {
      this.handler(previousResults, (errors, results) => {
        if(!errors) {
          if(this.next) {
            if(Array.isArray(this.nextHandler)) {
              if(3 !== this.nextHandler.length) {
                const errorText = 'nextHandler Array must be of size 3';
                console.log(errorText.red);
                this.cb(new Error(errorText));
              }
              else {
                if(this.nextHandler[1]) {
                  this.handler = this.nextHandler[1].bind(this.object);
                  this.nextHandler = this.nextHandler[2];
                  this.then(results);
                }
                else {
                  this.next.then(results);
                }
              }
            }
            else {
              this.next.then(results);
            }
          }
        }
        else {
          if(undefined !== this.nextHandler) {
            if(Array.isArray(this.nextHandler)) {
              if(3 !== this.nextHandler.length) {
                const errorText = 'nextHandler Array must be of size 3';
                console.log(errorText.red);
                this.cb(new Error(errorText));
              }
              else {
                this.handler = this.nextHandler[0].bind(this.object);
                this.nextHandler = this.nextHandler[2];
                this.then(results);
              }
            }
            else {
              this.handler = this.nextHandler.bind(this.object);
              this.nextHandler = undefined;
              this.then(results);
            }
          }
          else {
            this.cb(errors);
          }
        }
      }, ...this.params);
    }
    else {
      this.cb();
    }
  }
}

module.exports = TaskChain;
