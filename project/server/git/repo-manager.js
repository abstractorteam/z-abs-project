
'use strict';

const Fs = require('fs');
const Path = require('path');
const _ReposJsonPath = Path.normalize(`..${Path.sep}Data${Path.sep}actorjs-data-global${Path.sep}repos.json`);


class RepoManager {
  constructor() {
    this.repos = null;
    this.error = null;
    this._load();
    this.repoCache = new Map();
    this.cb = null;
    this.done = null;
  }
  
  init(cb, done) {
    if(this.error) {
      return cb(this.error);
    }
    else if(this.repos) {
      return done();
    }
    else {
      this.cb = cb;
      this.done = done;
    }
  }
  
  get(repoName) {
    const repoData = this.repoCache.get(repoName);
    if(repoData) {
      return repoData;
    }
    else {
      console.log('Repo '.red + repoName + ' is not configured in '.red + _ReposJsonPath + '.'.red);
    }
  }
  
  forEach(cb) {
    Reflect.ownKeys(this.repos).forEach((key) => {
      const repoGroup = Reflect.get(this.repos, key);
      const path = repoGroup.path;
      repoGroup.repos.forEach((repoData) => {
        cb({
          path: path,
          data: repoData
        });
      });
    });
  }
  
  _parse(data) {
    try {
      this.repos = JSON.parse(data);
    }
    catch(err) {
      console.log(`Can not parse file '${_ReposJsonPath}'.`, err);
    }
  }
  
  _load() {
    Fs.readFile(_ReposJsonPath, (err, data) => {
      if(err) {
        this.error = `Could not read file: '${_ReposJsonPath}'`;
        console.log(this.error);
        this.cb && this.cb(this.error);
      }
      else {
        this._parse(data);
        if(this.repos) {
          Reflect.ownKeys(this.repos).forEach((key) => {
            const repoGroup = Reflect.get(this.repos, key);
            const path = repoGroup.path;
            repoGroup.repos.forEach((repoData) => {
              this.repoCache.set(repoData.folder, {
                path: path,
                data: repoData,
                packageJson: null
              });
            });
          });
          this.done && this.done();
        }
        else {
          this.cb && this.cb(this.error);
        }
      }
    });
  }
}

module.exports = RepoManager;
