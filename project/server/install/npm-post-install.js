
const GitSimple = require('simple-git');
const Colors = require('colors');
const ChildProcess = require('child_process');
const Fs = require('fs');
const Path = require('path');

function readFileJson(file, cbRepoData) {
  Fs.readFile(file, (err, data) => {
    if(err) {
      const errText = `Could not read file: '${file}'`;
      console.log(errText);
      return cbRepoData(err);
    }
    let dataObject = null;
    try {
      dataObject = JSON.parse(data);
    }
    catch(err) {
      console.log(`Could not parse file: '${file}'`, err);
      return cbRepoData(err);
    }
    cbRepoData(undefined, dataObject);
  });
}

function bashAlias(cb) {
  if('linux' === process.platform) {
    const exec=require('child_process').exec;
    console.log("execute ./preinstall_linux.sh");
    exec("./preinstall_linux.sh", (err, stdout, stderr) => {
      if(err) {
        console.log(stderr);
        throw new Error("Could not install linux specifics.");
      }
      console.log(stdout);
      cb(err);
    });
  }
  else {
    cb();
  }
}

function createFiles(name, longName, shortName, cb) {
  if('win32' === process.platform) {
    const batFile = `

@echo off
SET UV_THREADPOOL_SIZE=128 && node node_modules\\gulp\\bin\\gulp.js %* --silent 

`;
    Fs.writeFile(`${longName}.bat`, batFile, 'utf8', (err) => {
      if(err) {
        console.log(`Could not create ${longName}.bat`);
        return cb(err);
      }
      Fs.writeFile(`${shortName}.bat`, batFile, 'utf8', (err) => {
        if(err) {
          console.log(`Could not create ${shortName}.bat`);
        }
        cb(err);
      });
    });
  }
  else if('linux' === process.platform) {
    const appScriptName = `${longName}.sh`;
    const appFile = `#!/bin/bash

node ./node_modules/gulp/bin/gulp.js $@ --silent
`;
    let pendings = 2;
    Fs.writeFile(appScriptName, appFile, (err) => {
      if(err) {
        console.log(`Could not create ${appScriptName}`);
        if(0 === --pendings) {
          cb(err);
        }
      }
      Fs.chmod(appScriptName, '0777', (err) => {
        if(0 === --pendings) {
          cb();
        }
      });
    });
    const preinstallScriptName = 'preinstall_linux.sh';
    const preinstallFile = `#!/usr/bin/env bash

X=$(find ~/.bashrc -type f -print | xargs grep "${longName}")
echo $X

if [ -z "$X" ]
then
  echo '' >> ~/.bashrc
  echo '# ${name} aliases' >> ~/.bashrc
  echo "alias ${longName}='./${longName}.sh'" >> ~/.bashrc
  echo "alias ${shortName}='./${longName}.sh'" >> ~/.bashrc
  echo '.bashrc updated'  
else
  echo '.bashrc not updated with ${name} aliases'  
fi
`;
    
    Fs.writeFile(preinstallScriptName, preinstallFile, (err) => {
      if(err) {
        console.log(`Could not create ${longName}.sh`);
        if(0 === --pendings) {
          cb(err);
        }
      }
      else {
        Fs.chmod(preinstallScriptName, '0777', (err) => {
          if(err) {
            Fs.rm(preinstallScriptName, { force: true}, (err) => {
              if(0 === --pendings) {
                cb(err);
              }
            });
          }
          else {
            bashAlias((err) => {
              if(err) {
                if(0 === --pendings) {
                  cb();
                }
              }
              else {
                Fs.rm(preinstallScriptName, { force: true}, (err) => {
                  if(err) {
                    console.log(`Could not remove '${preinstallScriptName}'`);
                  }
                  if(0 === --pendings) {
                    cb(err);
                  }
                }); 
              }
            });
          }
        });
      }
    });
  }
}

function contentDcumentationDataRepos(name, uri, root, cb) {
  const dirRoot = `..${Path.sep}${root}`;
  const dirRepo = `${dirRoot}${Path.sep}${name}`;
  const repoUrl = `https://${uri}/${name}.git`;
  console.log('CDD Repo:', repoUrl);
  isRepo(dirRepo, (err, isRepo) => {
    if(err) {
      if('ENOENT' === err.code || -2 === err.errno) {
        Fs.mkdir(dirRepo, {recursive: true}, (e) => {
          if(e && -4075 !== e.errno) {
            console.error(`mkdir ${dirRepo}: ${e}`);
            cb();
          }
          else {
            cloneRepo(dirRoot, repoUrl, cb);
          }
        });
      }
      else {
        console.error(`isRepo: ${err}`);
        cb();
      }
    }
    else if(!isRepo) {
		console.log('CDD Repo:', repoUrl, 'does not exist.');
      cloneRepo(dirRoot, repoUrl, cb);
    }
    else {
		console.log('CDD Repo:', repoUrl, 'already exist.');
      cb();
    }
  });
}

function startRepos(name, cb) {
  const dir = `..${Path.sep}${name}`;
  isRepo(dir, (err, isRepo) => {
    if(!err && isRepo) {
      installRepo(isRepo, dir, cb);
    }
    else {
      cb();
    }
  });
}

function readFileJson(file, cbRepoData) {
  Fs.readFile(file, (err, data) => {
    if(err) {
      const errText = `Could not read file: '${file}'`;
      console.log(errText);
      return cbRepoData(err);
    }
    let dataObject = null;
    try {
      dataObject = JSON.parse(data);
    }
    catch(err) {
      console.log(`Could not parse file: '${file}'`, err);
      return cbRepoData(err);
    }
    cbRepoData(undefined, dataObject);
  });
}

function cloneRepo(dir, repo, cb) {
  console.log('git clone repo ' + repo);
  GitSimple(dir).clone(repo, (err, result) => {
    console.log('git clone repo ' + repo + (err ? ', FAILURE: '.red + err.message : ' SUCCESS'.green) + (result ? ', result: ' + result : ''));
    cb();
  });
}

function isRepo(dir, handle) {
  Fs.lstat(dir, (err, stats) =>  {
    if(err) {
      handle && handle(err, false);
    }
    else {
      if(stats.isDirectory()) {
        GitSimple(dir).checkIsRepo((err, result) => {
          handle && handle(err, result);
        });
      }
      else {
        handle && handle(new Error('Not a directory.'));
      }
    }
  });
}

function installRepo(isRepo, dir, cb) {
  if(isRepo) {
    console.log('npm install ' + dir);
    try {
      ChildProcess.exec('npm install', {cwd: dir}, (err) => {
        if(err) {
          console.log('npm install ' + dir, 'FAILURE'.red, err);
        }
        else {
          console.log('npm install ' + dir + ' SUCCESS'.green);
        }
        cb(err);
      });
    } catch (err) {
      console.error('npm install:', dir, 'FAILURE'.red, err);
      cb(err);
    }
  }  
}


readFileJson('package.json', (err, json) => {
  console.log('******************************'.yellow); 
  console.log(`Installing ${json.name } on`.yellow, process.platform.yellow);
  console.log('******************************'.yellow);
  
  if(err) {
    return console.log('could not read the package.json file.', err);
  }
  createFiles(json.name, json.longname, json.shortname, (err) => {
    const dependencyValue = Reflect.get(json.dependencies, 'z-abs-local-server');
    const index = dependencyValue.indexOf('gitlab.com/abstraktor-public');
    const uri = -1 !== index ? 'gitlab.com/abstraktor-public' : 'bitbucket.org/abstractorteam';
  
    console.log(uri);
  
    let pendings = 6;
    startRepos('z-abs-project', () => {
      if(0 === --pendings) {
        console.log('');
      }
    });
    startRepos('z-abs-local-server', () => {
      if(0 === --pendings) {
        console.log('');
      }
    });

    contentDcumentationDataRepos('actorjs-content-global', uri, 'Content', () => {
      if(0 === --pendings) {
        console.log('');
      }
    });
    contentDcumentationDataRepos('actorjs-data-global', uri, 'Data', () => {
      if(0 === --pendings) {
        console.log('');
      }
    });
    contentDcumentationDataRepos('actorjs-documentation-bin', uri, 'Documentation', () => {
      if(0 === --pendings) {
        console.log('');
      }
    });
    contentDcumentationDataRepos('actorjs-documentation-text', uri, 'Documentation', () => {
      if(0 === --pendings) {
        console.log('');
      }
    });
  });
});
