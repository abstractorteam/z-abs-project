
'use strict';

const Gulp = Reflect.get(global, 'gulp@abstractor');
const Fs = require('fs');


class Task {
  constructor(part, fileName) {
    this.name = `task:${part}`;
    this.fileName = part.replace(/[/\\?%*:|"<>]/g, '-');
    this.requireName = fileName.substring(__dirname.length + 1, fileName.length - '.js'.length);
  }
  
  static load(fileName, requireName, cb) {
    Fs.readFile(`./workspace/${fileName}.tsk`, (err, data) => {
      if(err) {
        return cb(err);
      }
      const dataObject = JSON.parse(data);
      const keys = Reflect.ownKeys(dataObject);
      const parameters = [];
      let dependencyNames = [];
      keys.forEach((key) => {
        if('dependencyNames' !== key) {
          parameters.push(Reflect.get(dataObject, key));
        }
        else {
          dependencyNames = Reflect.get(dataObject, key);
        }
      });
      const P = require('./' + requireName);
      cb(err, new P(...parameters), dependencyNames);
    });
  }
  
  save() {
    Fs.writeFile(`./workspace/${this.fileName}.tsk`, JSON.stringify(this.saveData(), null, 2), (err) => {
      if(err) {
        console.log('ERROR:'. err);
      }
    });
  }
  
  init() {
    Gulp.task(this.name, this.executeTask.bind(this));
  }
  
  executeTask(cb) {
    this.task(cb);
  }
}


module.exports = Task;
